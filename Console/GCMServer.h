#pragma once

class GCMServer
{
private:
	const static std::string m_strProjectId;
	const static std::string m_strApiKey;

	GCMServer();
	~GCMServer();

	static void URIEncode(_IN_ const char* _pszBuf, _OUT_ char** _pszOut);

public:
	static bool Push(const char* _pszRegid, const std::vector<std::tuple<std::string, std::string>>& _rItems);
	static bool Push(const char* _pszRegid, std::string _strTag, std::string _strData);
};