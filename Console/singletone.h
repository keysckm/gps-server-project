#ifndef __SINGLETONE_H__
#define __SINGLETONE_H__

template <typename Ty>
class Singletone
{
private:
	Singletone(const Singletone&){}
	Singletone(const Singletone&&){}
	Singletone& operator = (const Singletone&){}

protected:
	Singletone(){}
	~Singletone(){}

public:
	static Ty& GetInstance()
	{
		static Ty me;
		return me;
	}

	static const Ty& GetConst()
	{
		return const Ty::GetInstance();
	}
};

#endif