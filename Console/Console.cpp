#include "stdafx.h"

void Run()
{
	// 타이틀 텍스트 설정
	SetConsoleTitle(_T("Server Console"));

	// 로그 저장 위치 출력
	TCHAR szLogPath[MAX_PATH];
	Log::GetLogPath(szLogPath, MAX_PATH);
	Log::i(_T("로그 쓰기 시작"));
	Log::v(_T("로그 경로: %s"), szLogPath);

	Server::GetServer().Run();
}

void Stop_server()
{
	Server::GetServer().GetIoService().stop();
	MariaDB::Disconnect();
	Log::i(_T("서버가 종료되었습니다."));
}

// 콘솔 컨트롤 핸들러가 호출할 함수
boost::function0<void> console_ctrl_function;

// Console Handler
BOOL WINAPI console_ctrl_handler(DWORD ctrl_type)
{
	switch (ctrl_type)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_SHUTDOWN_EVENT:
		// 콘솔에서 다음 컨트롤 타입 이벤트가 생기면 호출된다.
		console_ctrl_function();
		return TRUE;
	default:
		return FALSE;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	console_ctrl_function = boost::bind(&Stop_server);
	SetConsoleCtrlHandler(console_ctrl_handler, TRUE);

	Run();

	system("PAUSE");

	return 0;
}