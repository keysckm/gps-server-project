#pragma once

typedef __int64 TOKEN;

class TokenGenerator
{
private:
	TokenGenerator(){}
	~TokenGenerator(){}

public:

	static TOKEN Generate();
};