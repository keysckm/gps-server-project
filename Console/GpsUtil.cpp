#include "stdafx.h"
#include "GpsUtil.h"

void GPS::GPS_UTIL::GetFields(_IN_ char* _pszData, _OUT_ char** _pszFields, size_t _nFIeldCnt, size_t _nDataLen)
{
	SAFE_PTR(_pszData);
	SAFE_PTR(_pszFields);
	size_t nField = 0;
	size_t nDataPos = 0;
	_pszFields[nField++] = &_pszData[nDataPos];

	for (; nDataPos < _nDataLen; nDataPos++)
	{
		if (_pszData[nDataPos] == ',')
		{
			_pszData[nDataPos] = '\0';
			_pszFields[nField] = &_pszData[nDataPos + 1];
			if (++nField >= _nFIeldCnt)
				return;	// 필드 수 초과
		}
	}

	for (; nField < _nFIeldCnt; nField++)
		_pszFields[nField] = nullptr;
}

int GPS::GPS_UTIL::GetInt(_IN_ char* _pszBuf, int _default /* = 0 */)
{
	SAFE_PTR(_pszBuf) _default;
	if (_pszBuf[0])
		return atoi(&_pszBuf[0]);

	return _default;
}

float GPS::GPS_UTIL::GetFloat(_IN_ char* _pszBuf, float _default /* = 0. */)
{
	SAFE_PTR(_pszBuf) _default;
	if (_pszBuf[0])
		return atof(_pszBuf);
	return _default;
}

char GPS::GPS_UTIL::GetChar(_IN_ char* _pszBuf, char _default /* = '\0' */)
{
	SAFE_PTR(_pszBuf) _default;
	if (_pszBuf[0])
		return _pszBuf[0];
	return _default;
}

void GPS::GPS_UTIL::GetString(_IN_ char* _pszData, char* _pszBuf, int _nBufSize)
{
	SAFE_PTR(_pszData);
	SAFE_PTR(_pszBuf);

	memset(_pszBuf, '\0', _nBufSize);
	for (int i = 0; _pszData[i] && i < _nBufSize; i++)
		_pszBuf[i] = _pszData[i];
}

float GPS::GPS_UTIL::GetLatitude(_IN_ char* _pszBuf, float _default /* = 0. */)
{
	SAFE_PTR(_pszBuf) _default;
	if (strlen(_pszBuf) > 2)
	{
		float latitude = atof(_pszBuf + 2) / 60.0;
		_pszBuf[2] = '\0';
		latitude += atof(_pszBuf);
		return latitude;
	}

	return _default;
}

float GPS::GPS_UTIL::GetLongitude(_IN_ char* _pszBuf, float _default /* = 0. */)
{
	SAFE_PTR(_pszBuf) _default;
	if (strlen(_pszBuf) > 2)
	{
		float latitude = atof(_pszBuf + 3) / 60.0;
		_pszBuf[3] = '\0';
		latitude += atof(_pszBuf);
		return latitude;
	}

	return _default;
}

void GPS::GPS_UTIL::__nmea(_IN_ char* _pszCmd, _IN_ char* _pszData, size_t _nDataLen)
{
	SAFE_PTR(_pszData);
	SAFE_PTR(_pszCmd);

	GPS::BASE* pBase = nullptr;
	if (!strcmp(_pszCmd, "GPRMC"))	pBase = new GPRMC;
	else return;	// GPRMC 이외는 모두 무시한다.

	pBase->Parse(_pszData, _nDataLen);

	// 파싱되어 정리된 데이터를 등록한다.
	// 나중에 데이터베이스 부분을 만들고, 데이터베이스에 값을 등록하도록 변경해야함
	m_GpsList.push_back(pBase);
}

void GPS::GPS_UTIL::Revision_time(time& _rT, date& _rD, int _nOffset)
{
	_rT.hh += _nOffset;	// 세계표준시에 UTC Offset을 적용하여 실제 지역 시간으로 보정
	if (_rT.hh >= 24)	// 보정된 시간이 23시 59분 59초를 초과했다면
	{
		_rT.hh %= 24;
		_rD.dd += 1;

		// 만약 일자가 해당 달의 최대 일자를 오버했다면 보정
		switch (_rD.mm)
		{
		case 2:	// 2월은 윤달인 경우와 그렇지 않은 경우의 일수에 차이가 있음
			if (_rD.yy % 100 != 0 && (_rD.yy % 4 == 0 || _rD.yy % 400 == 0))	// 윤달은 100의 배수가 아니며 4의 배수이거나 400의 배수인 달
			{	// 윤달인 경우
				if (_rD.dd > 29)
				{
					_rD.dd %= 29;
					_rD.mm += 1;
				}
			}
			else // 그렇지 않은 경우
			{
				if (_rD.dd > 28)
				{
					_rD.dd %= 28;
					_rD.mm += 1;
				}
			}
			break;
			// 짝수달
		case 4:
		case 6:
		case 9:
		case 11:
			if (_rD.dd > 30)
			{
				if (_rD.dd > 30)
				{
					_rD.dd %= 30;
					_rD.mm += 1;
				}
			}
			break;
			// 홀수달
		default:
			if (_rD.dd > 31)
			{
				if (_rD.dd > 31)
				{
					_rD.dd %= 31;
					_rD.mm += 1;
				}
			}
			if (_rD.mm > 12)	// 만약 12월을 초과했다면 년수를 올리고 보정
			{
				_rD.mm %= 12;
				_rD.yy += 1;
			}
			break;
		}
	}
	else if (_rT.hh < 0)	// 보정결과 시각이 역행했다면
	{
		_rT.hh = 24 - _rT.hh;	// 24 - 역행한 시각을 하면 양수 시각이 짜잔!
		_rD.dd -= 1;	// 일수 뺴주고

		if (_rD.dd < 1)	// 0일은 없고.. 1일보다 작아졌다면 달수도 역행
		{
			_rD.mm -= 1;	// 달수 뺴고.. 보정을 시작한다!
			// 월 보정 전에 1월 미만이 되었다면 년수 보정하고 월수 재보정
			if (_rD.mm < 1)
			{
				_rD.mm = 12 - _rD.mm;
				_rD.yy -= 1;	// 년수 감소
			}

			switch (_rD.mm)
			{
			case 2:	// 2월은 윤달인 경우와 그렇지 않은 경우의 일수에 차이가 있음
				if (_rD.yy % 100 != 0 && (_rD.yy % 4 == 0 || _rD.yy % 400 == 0))	// 윤달은 100의 배수가 아니며 4의 배수이거나 400의 배수인 달
					_rD.dd = 29 - _rD.dd;
				break;

			case 4:		// 짝수달의 경우
			case 6:
			case 9:
			case 11:
				_rD.dd = 30 - _rD.dd;
				break;

			default:	// 홀수달의 경우
				_rD.dd = 31 - _rD.dd;
			}
		}
	}
}

bool GPS::GPS_UTIL::Parse(_IN_ char* _pszWGS84, size_t _nLen)
{
	SAFE_PTR(_pszWGS84) false;

	char szCommand[MAX_CMD_LEN] = { '\0', };
	char szBuf[MAX_BUF_LEN] = { '\0', };
	char szRecvChecksum[3] = { '\0', };
	size_t nReadPos = 0;
	size_t nBufPos = 0;
	size_t nCmdPos;
	size_t nChksumPos;
	PROC_STATE state = PS_SEARCH;
	unsigned char checksum;
	// NMEA 포멧 데이터를 시그니쳐 문자 $, comment, data, checksum으로 분해한다.
	while (nReadPos < _nLen)
	{
		char data = _pszWGS84[nReadPos++];
		switch (state)
		{
			case PS_SEARCH:	// signature token '$'를 찾는다.
				if (data == '$')	// 토근 발견
				{
					// 초기화
					nBufPos = 0;	// 버퍼 포지션 초기화
					memset(szCommand, '\0', MAX_CMD_LEN);	// command 버퍼 초기화
					memset(szBuf, '\0', MAX_BUF_LEN);		// data 버퍼 초기화
					memset(szRecvChecksum, '\0', 3);		// recv checksum 버퍼 초기화
					state = PS_COMMAND;	// next state
					checksum = 0;	// 체크섬 초기화
					nCmdPos = 0;	// command position 초기화
					nChksumPos = 0;	// checksum position 초기화
				}
				break;

			case PS_COMMAND:
				if (data != ',')
					szCommand[nCmdPos++] = data;
				else // field flag를 찾았음. data 파싱으로 이행
					state = PS_DATA;
				checksum ^= data;	// 체크섬 계산
				break;

			case PS_DATA:
				if (data == '*')	// checksum flag 발견
				{
					szBuf[nBufPos++] = '\0';	// 데이터 버퍼의 마지막에 널문자 삽입
					state = PS_CHECKSUM;		// 체크섬 비교로 이행
					break;
				}
				else
				{
					if (data == '\r')	// checksum flag가 발견되지 않은 상황에서 버퍼의 끝을 발견함
					{
						Log::w(_T("수신된 GPS NMEA 프로토콜 문자열에서 체크섬 필드를 찾을 수 없었습니다."));
						Log::v(_T("파싱된 데이터의 명확성을 장담할 수 없습니다."));
						szBuf[nBufPos++] = '\0';
						state = PS_PARSE;
						break;
					}
					
					szBuf[nBufPos] = data;
					if (++nBufPos >= MAX_BUF_LEN)
					{
						Log::w(_T("NMEA 문자열 파싱 버퍼가 꽉 찼습니다. 올바른 NMEA 문자열이 아닌 것 같습니다."));
						Log::v(_T(szBuf));
						Log::v(_T("지금까지 파싱한 내용을 버립니다."));
						state = PS_SEARCH;
						break;
					}
				}

				checksum ^= data;
				break;

			case PS_CHECKSUM:
				if (nChksumPos < 2)
					szRecvChecksum[nChksumPos++] = data;
				
				if (nChksumPos >= 2)
				{
					char nRecv = 0;
					auto lambda_str_to_hex = [](char data)->char{
						return (data - '0') < 10 ? data - '0' : data - 'A'+10;
					};
					nRecv = lambda_str_to_hex(szRecvChecksum[0]) << 4;
					nRecv += lambda_str_to_hex(szRecvChecksum[1]);

					if (checksum != nRecv)
					{
						Log::w(_T("수신된 NMEA 프로토콜 문자열의 체크섬이 실제 계산된 체크섬 결과값과 일치하지 않습니다."));
						Log::v(_T("수신된 체크섬: 0x%X, 계산된 체크섬: 0x%X"), nRecv, checksum);
					}

					state = PS_PARSE;
				}
				break;

			case PS_PARSE:
				__nmea(szCommand, szBuf, nBufPos);
				state = PS_SEARCH;
				break;
		}
	}

	return true;
}
bool GPS::GPS_UTIL::str_to_time(_IN_ char* _pszTime, size_t _nBufSize, _OUT_ time& _rt)
{
	SAFE_PTR(_pszTime) false;
	if(_nBufSize <= 10)
		return false;	// error

	char buf[4] = {'\0', };
	buf[0] = _pszTime[0];
	buf[1] = _pszTime[1];
	_rt.hh = atoi(buf);

	buf[0] = _pszTime[2];
	buf[1] = _pszTime[3];
	_rt.mm = atoi(buf);

	buf[0] = _pszTime[4];
	buf[1] = _pszTime[5];
	_rt.ss = atoi(buf);

	buf[0] = _pszTime[7];
	buf[1] = _pszTime[8];
	buf[2] = _pszTime[9];
	_rt.sss = atoi(buf);

	return true;
}

GPS::GPS_UTIL::~GPS_UTIL()
{
	for_each(m_GpsList.begin(), m_GpsList.end(), [](GPS::BASE* _pBase){
		SAFE_DEL(_pBase);
	});
}

bool GPS::GPS_UTIL::str_to_date(_IN_ char* _pszDate, size_t _nBufSize, _OUT_ date& _rd)
{
	SAFE_PTR(_pszDate) false;;
	if(_nBufSize < 6)
		return false;
	char buf[3] = {'\0', };

	buf[0] = _pszDate[0];
	buf[1] = _pszDate[1];
	_rd.dd = atoi(buf);

	buf[0] = _pszDate[2];
	buf[1] = _pszDate[3];
	_rd.mm = atoi(buf);

	buf[0] = _pszDate[4];
	buf[1] = _pszDate[5];
	_rd.yy = atoi(buf);

	return true;
}

bool GPS::GPGGA::Parse(_IN_ char* _pszData, size_t _nLen)
{
	SAFE_PTR(_pszData) false;

	memset(&m_data, 0, sizeof(m_data));	// data initialize

	char* pszFields[MAX_FIELD] = { nullptr, };

	GPS_UTIL::GetFields(_pszData, pszFields, MAX_FIELD, _nLen);
	GPS_UTIL::str_to_time(pszFields[0], strlen(pszFields[0])+1, m_data.utc);	// 세계표준시
	
	// 위도
	m_data.latitude = GPS_UTIL::GetLatitude(pszFields[1]);

	// 위도는 적도를 기준으로 북경 90˚, 남경 90˚
	// 북경: + / 남경: -
	m_data.ns_indicator = GPS_UTIL::GetChar(pszFields[2]);
	if (m_data.ns_indicator == 'S')
		m_data.latitude *= -1;	// 남경인 경우 음수로 전환

	// 경도
	m_data.longitude = GPS_UTIL::GetLongitude(pszFields[3]);
	// 경도는 본초자오선을 기준으로 좌우로 동경 180˚ 서경 180˚
	// 동경: + / 서경: -
	m_data.ew_indicator = GPS_UTIL::GetChar(pszFields[4]);
	if (m_data.ew_indicator == 'W')
		m_data.longitude *= -1;	// 서경인 경우 음수로 전환

	m_data.fix = (POSITION_FIX)GPS_UTIL::GetInt(pszFields[5]);
	m_data.satellites = GPS_UTIL::GetInt(pszFields[6]);

	m_data.hdop = GPS_UTIL::GetFloat(pszFields[7]);
	m_data.altitude = GPS_UTIL::GetFloat(pszFields[8]);
	m_data.altitude_unit = GPS_UTIL::GetChar(pszFields[9]);
	m_data.geoid_seperation = GPS_UTIL::GetFloat(pszFields[10]);
	m_data.sepration_unit = GPS_UTIL::GetChar(pszFields[11]);
	GPS_UTIL::str_to_time(pszFields[12], strlen(pszFields[12]), m_data.dpgs_age);
	GPS_UTIL::GetString(pszFields[13], m_data.dpgs_stationid, 4);

	date temp{ 2000, 07, 26 };

	GPS_UTIL::Revision_time(m_data.utc, temp, 9);

	return true;
}

bool GPS::GPRMC::Parse(_IN_ char* _pszData, size_t _nLen)
{
	SAFE_PTR(_pszData) false;

	memset(&m_data, 0, sizeof(m_data));	// data initialize

	char* pszFields[MAX_FIELD] = { nullptr, };

	GPS_UTIL::GetFields(_pszData, pszFields, MAX_FIELD, _nLen);
	GPS_UTIL::str_to_time(pszFields[0], strlen(pszFields[0])+1, m_data.utc);	// 세계표준시
	m_data.state = (SATELLITE_STATE) GPS_UTIL::GetChar(pszFields[1], 'A');		// 유효성

	// 위도
	m_data.latitude = GPS_UTIL::GetLatitude(pszFields[2]);
	// 위도는 적도를 기준으로 북경 90˚, 남경 90˚
	// 북경: + / 남경: -
	m_data.ns_indicator = GPS_UTIL::GetChar(pszFields[3], 'N');
	if (m_data.ns_indicator == 'S')
		m_data.latitude *= -1;	// 남경인 경우 음수로 전환

	// 경도
	m_data.longitude = GPS_UTIL::GetLongitude(pszFields[4]);
	// 경도는 본초자오선을 기준으로 좌우로 동경 180˚ 서경 180˚
	// 동경: + / 서경: -
	m_data.ew_indicator = GPS_UTIL::GetChar(pszFields[5], 'E');
	if (m_data.ew_indicator == 'W')
		m_data.longitude *= -1;	// 서경인 경우 음수로 전환

	m_data.speed = GPS_UTIL::GetFloat(pszFields[6]);
	m_data.speed *= CONVERT_KNOT_KMh;	// knote to km/s

	m_data.course = GPS_UTIL::GetFloat(pszFields[7]);	// 진행방향
	GPS_UTIL::str_to_date(pszFields[8], strlen(pszFields[8])+1, m_data.date);
	
	// 나침반의 방향각 및 방향이지만 사용하지 않는다. 따라서 Not Control
	m_data.not_control_a = GPS_UTIL::GetFloat(pszFields[9]);
	m_data.not_control_b = GPS_UTIL::GetChar(pszFields[10], 'E');

	//GPS_UTIL::Revision_time(m_data.utc, m_data.date, 9);

	return true;
}

GPS::GPS_UTIL g_GpsUtil;