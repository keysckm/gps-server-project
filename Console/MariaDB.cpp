#include "stdafx.h"
#include "MariaDB.h"

MYSQL*			MariaDB::m_pConnect = nullptr;
MYSQL			MariaDB::m_conn;

std::mutex		MariaDB::m_Lock;

MariaDB::MariaDB()
{
}


MariaDB::~MariaDB()
{
}

bool MariaDB::Connect(const char* _szDBName /* = nullptr  */, const char* _szUser /* = nullptr  */, const char* _szPassword /* = nullptr  */, const char* _szHost /* = nullptr  */, const int _nPort /* = 0 */ )
{
	const char* szDB = _szDBName ? _szDBName : DB_NAME;
	const char* szUSER = _szUser ? _szUser : DB_USER;
	const char* szPASS = _szPassword ? _szPassword : DB_PASS;
	const char* szHOST = _szHost ? _szHost : DB_HOST;
	const int nPORT = _nPort != 0 ? _nPort : DB_PORT;

	// db connect
	m_pConnect = mysql_real_connect(&m_conn,
		szHOST, szUSER, szPASS, szDB, nPORT, nullptr, 0);

	if (!m_pConnect)
	{
		Log::e(_T("Failed Connect to Database."));
		Log::v(_T("errno: %d, errmsg: %s"),
			mysql_errno(&m_conn), mysql_error(&m_conn));

		return false;
	}

	SAFE_CONDI(!SetCharset(CHARSET_UTF8)) false;

	Log::i(_T("Connect to Database."));
	Log::v(_T("db: %s, host: %s, port: %d"), szDB, szHOST, nPORT);

	return true;
}

void MariaDB::Disconnect()
{
	SAFE_PTR(m_pConnect);

	mysql_close(m_pConnect);
	m_pConnect = nullptr;

	Log::i(_T("Disconnect to Database"));
}

bool MariaDB::SetCharset(const char* _szCharset)
{
	std::string charset = _szCharset;
	std::string query_base = "SET SESSION character_set_";
	std::string query_target[4] = { "connection=", "results=", "client=", "server=" };

	for (int i = 0; i < 4; i++)
	{
		if (!SendQuery((query_base + query_target[0] + charset).c_str()))
			return false;
	}

	return true;
}

bool MariaDB::IsConnected()
{
	return  m_pConnect ? true : false;
}

#define MUTEX_LOCK(mutex_object) std::lock_guard<std::mutex> guard_mutex(mutex_object)
bool MariaDB::SendQuery(const char* _pszQuery)
{
	MUTEX_LOCK(m_Lock);	// threadsafe를 위해 락
	if (mysql_query(m_pConnect, _pszQuery))
	{
		Log::e(_T("Refused database query."));
		Log::v(_T("query: %s"), _pszQuery);
		Log::v(_T("errno: %d, errmsg: %s"),
			mysql_errno(m_pConnect), mysql_error(m_pConnect));

		return false;
	}
	return true;
}


bool MariaDB::SendQuery(std::string& _rStr)
{
	return SendQuery(_rStr.c_str());
}

bool MariaDB::SendQuery(std::stringstream& _rSs)
{
	return SendQuery(_rSs.str().c_str());
}

bool MariaDB::SendQuery(_IN_ const char* _pszQuery, _OUT_ QueryResult& _rResult)
{
	MUTEX_LOCK(m_Lock);	// threadsafe를 위해 락
	if (mysql_query(m_pConnect, _pszQuery))
	{
		Log::e(_T("Refused database query."));
		Log::v(_T("query: %s"), _pszQuery);
		Log::v(_T("errno: %d, errmsg: %s"),
			mysql_errno(m_pConnect), mysql_error(m_pConnect));

		return false;
	}

	// 결과를 받는다.
	MYSQL_RES* pRes = mysql_store_result(m_pConnect);
	_rResult.Parse(pRes);		// 결과를 파싱

	mysql_free_result(pRes);	// 결과를 릴리즈
	return true;
}

bool MariaDB::SendQuery(_IN_ std::string& _rStr, _OUT_ QueryResult& _rResult)
{
	return SendQuery(_rStr.c_str(), _rResult);
}
bool MariaDB::SendQuery(_IN_ std::stringstream& _rSs, _OUT_ QueryResult& _rResult)
{
	return SendQuery(_rSs.str().c_str(), _rResult);
}