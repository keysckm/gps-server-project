#include "stdafx.h"
#include "Session.h"

Session::Session(boost::asio::io_service& _rIoServ)
	: m_Socket(_rIoServ)
	, m_strand(_rIoServ)
	, m_nPackMark(0)
	, m_isSend(false)
{
}

Session::~Session(void)
{
}

void Session::Init()
{
	m_RecvBuf.assign(0);	// 버퍼 초기화
	m_PackBuf.assign(0);

	PostRecv();				// 패킷 수신 시작
}

void Session::Stop()
{
	if(m_Socket.is_open())
		m_Socket.close();
}

void Session::PostSend(const char* _pSendBuf, size_t _nSendSize)
{
	char* pSendBuf = new char[_nSendSize];
	memcpy(pSendBuf, _pSendBuf, _nSendSize);	// 버퍼생성 후 복사

	m_QMutex.lock();
	m_pSendQ.push_back(pSendBuf);	// 큐에 삽입
	m_QMutex.unlock();
	if (m_isSend)	// 송신중인 데이터가 있음
		return;		// 이 경우 송신중인 데이터가 송신이 완료되면 큐에 저장된 데이터가 자동으로 보내짐

	
	boost::asio::async_write(m_Socket, boost::asio::buffer(pSendBuf, _nSendSize),
		m_strand.wrap(boost::bind(&Session::__handle_send, this,
		boost::asio::placeholders::error,
		boost::asio::placeholders::bytes_transferred)));
}

void Session::__handle_send(const boost::system::error_code& _rErr, const size_t _nBytes_transferred)
{
	SAFE_DEL_ARRAY(m_pSendQ.front());
	m_pSendQ.pop_front();	// 송신된 데이터를 큐에서 삭제한다.

	if (!m_pSendQ.empty())	// 큐에 보내야할 데이터가 남아있음
	{
		m_isSend = true;
		m_QMutex.lock();
		MSG_HEADER* pHeader = reinterpret_cast<MSG_HEADER*>(m_pSendQ.front());	// 큐에서 데이터를 뽑아낸다.
		m_QMutex.unlock();
		PostSend((char*)pHeader, pHeader->nSize);
	}
	else
	{
		m_isSend = false;
	}
}

void Session::PostRecv()
{
	// 버퍼의 총길이 - 현재 저장된 데이터의 크기만큼 수신
	m_Socket.async_read_some(boost::asio::buffer(m_RecvBuf, (MAX_RECV_BUF_LEN - m_nPackMark)), 
							m_strand.wrap(boost::bind(
							&Session::__handle_recv, this, 
							boost::asio::placeholders::error,
							boost::asio::placeholders::bytes_transferred)
							));
}

void Session::__handle_recv(const boost::system::error_code& _rErr, const size_t _nBytes_transferred)
{
	// 수신완료후 수신된 데이터를 처리한다.
	// 이 함수는 io_service 스레드에서 콜백
	if(_rErr)
	{
		if (_rErr == boost::asio::error::eof)
			Log::e(_T("Disconnected Client"));
		else
			Log::e(_T("Receive Error"));
		
		Log::v(_T("Session: %s, Err: %d, Msg: %s"), GetIP().c_str(), _rErr.value(), _rErr.message().c_str());
		
		Server::GetServer().CloseSession(this);
	}
	else
	{
		Log::i(_T("Receive Data"));
		Log::v(_T("Session: %s, Size: %d"), GetIP().c_str(), _nBytes_transferred);

		// 받은 데이터를 패킷버퍼에 저장한다.
		memcpy(&m_PackBuf[m_nPackMark], m_RecvBuf.data(), _nBytes_transferred);

		// 지금까지 받은 데이터 크기와 새로 받은 데이터 크기를 합쳐
		// 지금 당장 저장된 데이터의 크기를 계산한다.
		size_t nPacketSize = m_nPackMark += _nBytes_transferred;	// 지금까지 받은 데이터 크기
		size_t nReadData = 0;	// 처리된 데이터 크기
			
		while (nPacketSize > 0)
		{
			// 버퍼의 데이터에서 패킷헤더를 가져온다.
			MSG_HEADER* pHeader = reinterpret_cast<MSG_HEADER*>(&m_PackBuf[nReadData]);

			if (nPacketSize < pHeader->nSize)
			{
				// 만약 저장된 데이터의 크기가 패킷의 크기보다 작다면
				Log::w(_T("More data is needed."));
				Log::v(_T("Session: %s, size: %d/%d byte"), GetIP().c_str(), nPacketSize, pHeader->nSize);

				break;
			}

			// 패킷 처리
			Server::GetServer().ProcessPacket(this, pHeader);
			nReadData += pHeader->nSize;
			nPacketSize -= pHeader->nSize;
			Log::i(_T("Processed Packet."));
			Log::v(_T("Session: %s, processed: %d byte, more: %d byte"), GetIP().c_str(), nReadData, nPacketSize);
		}

		if(nPacketSize > 0)
		{
			// 패킷버퍼에 있는 데이터를 맨 앞으로 땡긴다.
			char szBuf[MAX_RECV_BUF_LEN] = {0, };
			memcpy(szBuf, &m_PackBuf[nReadData], nPacketSize);
			memcpy(m_PackBuf.data(), szBuf, nPacketSize);
		}

		m_nPackMark = nPacketSize;

		// Recv 실행
		PostRecv();
	}
}