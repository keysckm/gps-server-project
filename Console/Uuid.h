#ifndef __UUID_H__
#define __UUID_H__

#include "stdafx.h"
class UUID
{
private:
	std::string m_UUID;

public:
	UUID()
	{
	}

	UUID(const char* _pUUID)
	{
		SetUUID(_pUUID);
	}

	const std::string& GetUUID() const
	{
		return m_UUID;
	}

	void SetUUID(const char* _pUUID)
	{
		char buf[MAX_UUID_LEN + 1] = { '\0', };
		memcpy(buf, _pUUID, sizeof(char) * MAX_UUID_LEN);
		m_UUID = buf;
	}
};

#endif