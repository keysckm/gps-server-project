#pragma once
#pragma pack(push, 1)

#include "stdafx.h"

// 메시지 인덱스
// 100번대 -> REQUEST MESSAGE(Client->Server)		-> 요청
// 200번대 -> RESPONSED MESSAGE(Server->Client)		-> 결과
// 300번대 -> Client To Server Message				-> 데이터
enum MSG_IDX{	// packet index
	MSG_UNKNOWN = 0,

	REQ_ACCESS = 100,							// 앱 실행시 기본정보를 서버로 송신
	REQ_SIGNUP = REQ_ACCESS + 1,				// 회원가입
	REQ_LOGIN = REQ_SIGNUP + 1,					// 로그인
	REQ_TOKENLOGIN = REQ_LOGIN + 1,				// 토큰 로그인
	REQ_LOGOUT = REQ_TOKENLOGIN + 1,			// 로그아웃
	REQ_LOCATIONDATA = REQ_LOGOUT + 1,			// GPS데이터 요청
	REQ_GETALARMZONE = REQ_LOCATIONDATA + 1,	// 알람구역 요청
	REQ_ADDALARMZONE = REQ_GETALARMZONE + 1,	// 알람구역 추가
	REQ_DELALARMZONE = REQ_ADDALARMZONE + 1,	// 알람구역 삭제
	REQ_MODALARMZONE = REQ_DELALARMZONE + 1,	// 알람구역 수정
	REQ_ADDALARMTARG = REQ_MODALARMZONE + 1,	// 알람구역에 따라 푸시를 받을 등록자 추가
	REQ_DELALARMTARG = REQ_ADDALARMTARG + 1,	//				"					  삭제
	REQ_MODALARMTARG = REQ_DELALARMTARG + 1,	//				"					  수정
	REQ_GETMYDATA = REQ_MODALARMTARG + 1,		// 나 자신에 대한 데이터 요청
	REQ_FINDFRIEND = REQ_GETMYDATA + 1,			// 친구 검색
	REQ_ADDFRIEND = REQ_FINDFRIEND + 1,			// 친구 추가 요청
	REQ_GETFRIENDLIST = REQ_ADDFRIEND + 1,		// 친구 리스트 요청
	REQ_GETFRIENDREQLIST = REQ_GETFRIENDLIST + 1,	// 친구 추가 요청 리스트를 요청(...)
	REQ_RESPFRIENDREQ = REQ_GETFRIENDREQLIST + 1,	// 친구 요청에 대한 응답

	RESP_RESULT = 200,						// 받은 요청에 대한 결과
	RESP_DEVICETOKEN = RESP_RESULT + 1,		// 디바이스 토큰
	RESP_USERTOKEN = RESP_DEVICETOKEN + 1,	// 유저 토큰
	RESP_TOKEN = RESP_USERTOKEN + 1,		// 로그인토큰 송신
	RESP_LOCATIONDATA = RESP_TOKEN + 1,		// GPS 데이터
	RESP_ALARMZONE = RESP_LOCATIONDATA + 1,	// ALARMZONE 데이터
	RESP_ALARMTARG = RESP_ALARMZONE + 1,	// ALARMZONE 대상자 데이터 송신
	RESP_ADDALARMZONE = RESP_ALARMTARG + 1,	// 알람존 추가에 대한 답신
	RESP_GETMYDATA = RESP_ADDALARMZONE + 1,		// REQ_GETMYDATA에 대한 답신
	RESP_GETFRIENDLIST = RESP_GETMYDATA + 1,	// 친구 리스트 요청에 대한 답신
	RESP_GETFRIENDREQLIST = RESP_GETFRIENDLIST + 1,	// 친구 추가 요청 리스트에 대한 요청의 답신... 뭐 말이 이상하노	

	CTS_GPSDATA = 300,				// GPS데이터
	CTS_ECALL = CTS_GPSDATA + 1,	// 비상호출
	CTS_REGID = CTS_ECALL + 1,		// REGID
};

// 서버로부터 클라이언트로 보내는 결과 통지
enum REQUEST_RESULT{
	UNKNOWN = 0,						// 알 수 없는 요청
	SERVERERR = UNKNOWN + 1,			// 서버 에러
	APPROVED = SERVERERR + 1,			// 승인
	REFUSAL = APPROVED + 1,				// 거절
	INVALIDTOKEN = REFUSAL + 1,			// 무효한 토큰
	DEVICETOKEN = INVALIDTOKEN + 1,		// 새로운 디바이스 토큰
	USERTOKEN = DEVICETOKEN + 1,		// 새로운 유저 토큰
	NOTFOUND = USERTOKEN + 1,			// 찾을 수 없음
	ALREADY = NOTFOUND + 1,				// 이미 되어있음
};


struct DATETIME_STRING
{
	char datetime[MAX_DATETIME_LEN];

	DATETIME_STRING()
	{
		memset(datetime, 0, sizeof(char) * MAX_DATETIME_LEN);
	}
	DATETIME_STRING(const DATETIME_STRING& _rSrc)
	{
		memcpy(datetime, _rSrc.datetime, sizeof(char) * MAX_DATETIME_LEN);
	}

	std::string GetString() const
	{
		char strDateTime[MAX_DATETIME_LEN + 1] = { '\0', };
		memcpy(strDateTime, datetime, MAX_DATETIME_LEN);
		return std::string(strDateTime);
	}
};


//////////////////////////////////////////////////////////////////////////
// 헤더
//////////////////////////////////////////////////////////////////////////

struct MSG_HEADER
{
	MSG_IDX			nIdx;			// 패킷 커멘드
	unsigned int	nSize;			// 패킷 사이즈

	MSG_HEADER(MSG_IDX _nIdx)
		: nIdx(_nIdx)
		, nSize(0)
	{}	// 사이즈를 따로 지정해줘야함

	MSG_HEADER(MSG_IDX _nIdx, unsigned int _nSize)
		: nIdx(_nIdx)
		, nSize(_nSize)
	{
	}
};

//////////////////////////////////////////////////////////////////////////
// REQ 메시지
//////////////////////////////////////////////////////////////////////////

struct MSG_REQ_ACCESS : public MSG_HEADER
{
	// DEVICE TOKEN이 0이라면 USER TOKEN을 무시하고 처음 접속한것으로 침
	// DEVICE TOKEN은 있는데 USER TOKEN이 0이라면 로그인을 하라고 해야함
	TOKEN nDevice;
	TOKEN nUser;

	MSG_REQ_ACCESS()
		: MSG_HEADER(REQ_ACCESS, sizeof(MSG_REQ_ACCESS))
		, nDevice(0)
		, nUser(0)
	{
	}
};
struct MSG_REQ_SIGNUP : public MSG_HEADER	// 회원가입
{
	int id_len;
	int name_len;
	char Password[MAX_PW_LEN];	// 40자 고정
	char buf[MAX_ID_LEN + MAX_NAME_LEN];	// 닉네임 및 ID 합성 버퍼. 이런식으로 처리하는게 낫겠다.

	MSG_REQ_SIGNUP(const char* _pszId, const char* _pszPw, const char* _pszUuid)
		: MSG_HEADER(REQ_SIGNUP, sizeof(MSG_REQ_SIGNUP))
		, id_len(0)
	{
		memset(Password, 0, sizeof(char) * (MAX_PW_LEN));
		memset(buf, 0, sizeof(char) * (MAX_ID_LEN + MAX_NAME_LEN));
	}
};
struct MSG_REQ_LOGIN : public MSG_HEADER	// 로그인
{
	int id_len;
	char Password[MAX_PW_LEN];
	char Email[MAX_ID_LEN];

	MSG_REQ_LOGIN(const char* _pszId, const char* _pszPw)
		: MSG_HEADER(REQ_LOGIN, sizeof(MSG_REQ_LOGIN))
		, id_len(0)
	{
		memset(Password, 0, sizeof(char) * (MAX_PW_LEN));
		memset(Email, 0, sizeof(char) * (MAX_ID_LEN));
	}
};
struct MSG_REQ_TOKENLOGIN : public MSG_HEADER
{
	MSG_REQ_TOKENLOGIN()
		: MSG_HEADER(REQ_TOKENLOGIN, sizeof(MSG_REQ_TOKENLOGIN))
	{}
};
struct MSG_REQ_LOGOUT : public MSG_HEADER
{
	MSG_REQ_LOGOUT()
		: MSG_HEADER(REQ_LOGOUT, sizeof(MSG_REQ_LOGOUT))
	{}
};
struct MSG_REQ_LOCATIONDATA : public MSG_HEADER		// 일정기간내의 GPS데이터 요청
{
	DATETIME_STRING	beginpoint;						// 여기부터
	DATETIME_STRING	endpoint;						// 여기까지 검색
// 	int IdxCnt;
// 	TOKEN* pIdxArry;

	MSG_REQ_LOCATIONDATA()
		: MSG_HEADER(REQ_LOCATIONDATA)
	{
	}

	~MSG_REQ_LOCATIONDATA()
	{
//		SAFE_DEL_ARRAY(pIdxArry);
	}

};
struct MSG_REQ_GETALARMZONE : public MSG_HEADER
{
	MSG_REQ_GETALARMZONE()
		: MSG_HEADER(REQ_GETALARMZONE, sizeof(MSG_REQ_GETALARMZONE))
	{
	}
};
struct MSG_REQ_ADDALARMZONE : public MSG_HEADER
{
	double	latitude;
	double	longitude;
	int		distance;
	char	nick[MAX_NICK_LEN];

	MSG_REQ_ADDALARMZONE()
		: MSG_HEADER(REQ_ADDALARMZONE, sizeof(MSG_REQ_ADDALARMZONE))
	{
		memset(nick, 0, sizeof(char) * MAX_NICK_LEN);
	}
};
struct MSG_REQ_MYDATA : public MSG_HEADER
{
	MSG_REQ_MYDATA()
		: MSG_HEADER(REQ_GETMYDATA, sizeof(MSG_REQ_MYDATA))
	{}
};
struct MSG_REQ_FINDFRIEND : public MSG_HEADER
{
	int email_len;
	char email[MAX_ID_LEN];

	MSG_REQ_FINDFRIEND()
		: MSG_HEADER(REQ_FINDFRIEND)
	{}
};
struct MSG_REQ_ADDFRIEND : public MSG_HEADER
{
	int email_len;
	char email[MAX_ID_LEN];

	MSG_REQ_ADDFRIEND()
		: MSG_HEADER(REQ_ADDFRIEND)
	{}
};
struct MSG_REQ_GETFRIENDREQLIST : public MSG_HEADER
{
	MSG_REQ_GETFRIENDREQLIST()
		: MSG_HEADER(REQ_GETFRIENDREQLIST)
	{}
};
struct MSG_REQ_GETFRIENDLIST : public MSG_HEADER
{
	MSG_REQ_GETFRIENDLIST()
		: MSG_HEADER(REQ_GETFRIENDLIST)
	{}
};
struct MSG_REQ_RESPFRIENDREQ : public MSG_HEADER	// 친구요청에 대한 대답
{
	REQUEST_RESULT	resp;
	int				nEmailLen;
	char			szMail[MAX_ID_LEN];

	MSG_REQ_RESPFRIENDREQ()
		: MSG_HEADER(REQ_RESPFRIENDREQ)
	{}
};

//////////////////////////////////////////////////////////////////////////
// RESP 메시지
//////////////////////////////////////////////////////////////////////////

struct MSG_RESP_RESULT : public MSG_HEADER
{
	MSG_IDX			idx;		// 어떤 행위에 대한 응답인가
	REQUEST_RESULT	result;

	MSG_RESP_RESULT(MSG_IDX _idx, REQUEST_RESULT _r)
		: MSG_HEADER(RESP_RESULT, sizeof(MSG_RESP_RESULT))
		, result(_r)
		, idx(_idx)
	{
	}
};
struct MSG_RESP_DEVICETOKEN : public MSG_HEADER
{
	TOKEN	device;
	MSG_RESP_DEVICETOKEN(TOKEN _nDevice)
		: MSG_HEADER(RESP_DEVICETOKEN, sizeof(MSG_RESP_DEVICETOKEN))
		, device(_nDevice)
	{}
};
struct MSG_RESP_USERTOKEN : public MSG_HEADER
{
	TOKEN	user;
	MSG_RESP_USERTOKEN(TOKEN _nUser)
		: MSG_HEADER(RESP_USERTOKEN, sizeof(MSG_RESP_USERTOKEN))
		, user(_nUser)
	{}
};
struct MSG_RESP_TOKEN : public MSG_HEADER
{
	MSG_RESP_TOKEN()
		: MSG_HEADER(RESP_TOKEN, sizeof(MSG_RESP_TOKEN))
	{
	}
};
struct MSG_RESP_LOCATIONDATA : public MSG_HEADER	// 클라이언트로 보내는 GPS데이터
{
	int				cnt;		// 데이터의 갯수
	LOCATIONDATA*	pList;		// 데이터 리스트 -> 그대로 보내면 난리남

	MSG_RESP_LOCATIONDATA(int _cnt, LOCATIONDATA* _list)
		: MSG_HEADER(RESP_LOCATIONDATA)
		, cnt(_cnt)
	{
		nSize = sizeof(MSG_HEADER) + sizeof(int) + (sizeof(LOCATIONDATA) * cnt);

		pList = new LOCATIONDATA[cnt];
		for (int i = 0; i < cnt; i++)
			pList[i] = _list[i];
	}

	~MSG_RESP_LOCATIONDATA()
	{
		SAFE_DEL_ARRAY(pList);
	}

	const char* GetBuffer() const
	{
		// 헤더의 길이 : 헤더크기 + 4byte(cnt)
		const size_t nHeaderSize = sizeof(MSG_HEADER) + sizeof(int);
		char* buf = new char[this->nSize];
		memcpy(&buf[0], this, nHeaderSize);	// 헤더 및 cnt 삽입
		memcpy(&buf[nHeaderSize], pList, sizeof(LOCATIONDATA) * cnt);	// 데이터 삽입

		return buf;
	}
};
struct MSG_RESP_ALARMZONE : public MSG_HEADER
{
	int nCnt;
	ALARMZONE* pList;

	MSG_RESP_ALARMZONE(int _cnt, ALARMZONE* _plist)
		: MSG_HEADER(RESP_ALARMZONE)
		, nCnt(_cnt)
		, pList(nullptr)
	{
		nSize = sizeof(MSG_HEADER) + sizeof(int) + (sizeof(ALARMZONE) * _cnt);
		pList = new ALARMZONE[_cnt];

		for (int i = 0; i < _cnt; i++)
			pList[i] = _plist[i];
	}

	~MSG_RESP_ALARMZONE()
	{
		SAFE_DEL_ARRAY(pList);
	}

	const char* GetBuffer() const
	{
		const size_t nHeaderSize = sizeof(MSG_HEADER) + sizeof(int);
		char* pBuf = new char[this->nSize];
		memcpy(&pBuf[0], this, nHeaderSize);
		memcpy(&pBuf[nHeaderSize], pList, sizeof(ALARMZONE) * nCnt);

		return pBuf;
	}
};
struct MSG_RESP_ADDALARMZONE : public MSG_HEADER
{
	TOKEN nIdx;	// 추가된 알람존의 인덱스
	
	MSG_RESP_ADDALARMZONE(long _nIdx)
		: MSG_HEADER(RESP_ADDALARMZONE, sizeof(MSG_RESP_ADDALARMZONE))
		, nIdx(_nIdx)
	{}
};
struct MSG_RESP_GETMYDATA : public MSG_HEADER
{
	TOKEN nUserToken;	// 유저 토큰
	int nId_size;		// 아이디 길이
	int nName_size;		// 이름 길이
	char buf[MAX_ID_LEN + MAX_NAME_LEN];

	MSG_RESP_GETMYDATA(TOKEN _nUserToken, int _nId_size, const char* _pszId, int _nName_size, const char* _pszName)
		: MSG_HEADER(RESP_GETMYDATA)
		, nUserToken(_nUserToken)
		, nId_size(_nId_size)
		, nName_size(_nName_size)
	{
		nSize = sizeof(MSG_HEADER) + 8 + 4 + 4 + _nId_size + _nName_size;
		memcpy(buf, _pszId, nId_size);
		memcpy(&buf[_nId_size], _pszName, nName_size);
	}

	~MSG_RESP_GETMYDATA()
	{
	}

	const char* GetBuffer() const
	{
		char* pNew = new char[nSize];
		size_t nFrontSize = sizeof(MSG_HEADER) + 8 + 4 + 4;	// pBuf앞까지의 크기
		memcpy(pNew, this, nFrontSize);	// pBuf 앞까지 복사
		memcpy(&pNew[nFrontSize], buf, nId_size + nName_size);	// 버퍼 내용 복사

		return pNew;
	}
};
struct FRIENDREQLIST
{
	int email_len;	// 이메일 길이
	int name_len;	// 이름 길이
	DATETIME_STRING datetime;	// 요청일시
	char buf[MAX_ID_LEN + MAX_NAME_LEN];	// 요청자 ID, 이름 합성버퍼

	FRIENDREQLIST()
		: email_len(0)
		, name_len(0)
	{
		memset(&datetime, 0, sizeof(DATETIME_STRING));
		memset(buf, 0, MAX_ID_LEN + MAX_NAME_LEN);
	}

	FRIENDREQLIST(int _email_len, int _name_len, DATETIME_STRING _datetime, const char* _pszEmail, const char* _pszName)
		: email_len(_email_len)
		, name_len(_name_len)
		, datetime(_datetime)
	{
		memcpy(buf, _pszEmail, _email_len);
		memcpy(&buf[_email_len], _pszName, _name_len);
	}

	FRIENDREQLIST(FRIENDREQLIST& _rRef)
		: email_len(_rRef.email_len)
		, name_len(_rRef.name_len)
		, datetime(_rRef.datetime)
	{
		memcpy(buf, _rRef.buf, _rRef.email_len + _rRef.name_len);
	}

	void operator =(FRIENDREQLIST& _rRef)
	{
		email_len = _rRef.email_len;
		name_len = _rRef.name_len;
		datetime = _rRef.datetime;
		memcpy(buf, _rRef.buf, _rRef.email_len + _rRef.name_len);
	}
};

struct MSG_RESP_GETFRIENDREQLIST : MSG_HEADER
{
	int nCnt;	// 갯수
	FRIENDREQLIST* pData;

	MSG_RESP_GETFRIENDREQLIST(FRIENDREQLIST* _pList, size_t _nCnt)
		: MSG_HEADER(RESP_GETFRIENDREQLIST)
		, nCnt(_nCnt)
		, pData(_pList)
	{
		pData = new FRIENDREQLIST[_nCnt];
		memcpy(pData, _pList, sizeof(FRIENDREQLIST) * _nCnt);
		nSize = sizeof(MSG_HEADER) + sizeof(int);
		for (int i = 0; i < _nCnt; i++)
		{
			// FRIENDREQLIST의 고정사이즈 = int + int + DATETIME_STRING(19) = 27
			// email, name 합성버퍼의 크기를 예상하여 삽입
			nSize += 27 + pData[i].email_len + pData[i].name_len;
		}
	}
	~MSG_RESP_GETFRIENDREQLIST()
	{
		SAFE_DEL_ARRAY(pData);
	}

	const char* GetBuffer()
	{
		char* pBuf = new char[nSize];
		size_t offset = sizeof(MSG_HEADER) + sizeof(int);
		memcpy(pBuf, this, sizeof(char) * offset);
		// 헤더 ~ nCnt까지 삽입

		for (int i = 0; i < nCnt; i++)
		{
			size_t data_size = 27 + pData[i].email_len + pData[i].name_len;
			memcpy(&pBuf[offset], &pData[i], data_size);	// 데이터 크기만큼 데이터 삽입
			offset += data_size;	// 삽입한 데이터 크기만큼 오프셋 증가
		}

		return pBuf;
	}
};
struct FRIENDLIST
{
	int email_len;	// 이메일 길이
	int name_len;	// 이름 길이
	int relation_len;	// 관계명칭
	DATETIME_STRING datetime;	// 요청일시
	char buf[MAX_ID_LEN + MAX_NAME_LEN + MAX_NAME_LEN];	// 요청자 ID, 이름, 관계 합성버퍼

	FRIENDLIST()
		: email_len(0)
		, name_len(0)
		, relation_len(0)
	{
		memset(&datetime, 0, sizeof(DATETIME_STRING));
		memset(buf, 0, MAX_ID_LEN + MAX_NAME_LEN);
	}

	FRIENDLIST(int _email_len, int _name_len, int _relation_len, DATETIME_STRING _datetime, const char* _pszEmail, const char* _pszName, const char* _pszRelation)
		: email_len(_email_len)
		, name_len(_name_len)
		, relation_len(_relation_len)
		, datetime(_datetime)
	{
		memcpy(buf, _pszEmail, _email_len);
		memcpy(&buf[_email_len], _pszName, _name_len);
		memcpy(&buf[_email_len + _name_len], _pszRelation, relation_len);
	}

	FRIENDLIST(FRIENDLIST& _rRef)
		: email_len(_rRef.email_len)
		, name_len(_rRef.name_len)
		, relation_len(_rRef.relation_len)
		, datetime(_rRef.datetime)		
	{
		memcpy(buf, _rRef.buf, _rRef.email_len + _rRef.name_len + _rRef.relation_len);
	}

	void operator =(FRIENDLIST& _rRef)
	{
		email_len = _rRef.email_len;
		name_len = _rRef.name_len;
		datetime = _rRef.datetime;
		relation_len = _rRef.relation_len;
		memcpy(buf, _rRef.buf, _rRef.email_len + _rRef.name_len + _rRef.relation_len);
	}
};
struct MSG_RESP_GETFRIENDLIST : public MSG_HEADER
{
	int nCnt;	// 갯수
	FRIENDLIST* pData;

	MSG_RESP_GETFRIENDLIST(FRIENDLIST* _pList, size_t _nCnt)
		: MSG_HEADER(RESP_GETFRIENDLIST)
		, nCnt(_nCnt)
		, pData(_pList)
	{
		pData = new FRIENDLIST[_nCnt];
		memcpy(pData, _pList, sizeof(FRIENDLIST) * _nCnt);
		nSize = sizeof(MSG_HEADER) + sizeof(int);
		for (int i = 0; i < _nCnt; i++)
		{
			// FRIENDLIST의 고정사이즈 = int + int + int + DATETIME_STRING(19) = 27
			// email, name, relationship 합성버퍼의 크기를 예상하여 삽입
			nSize += 31 + pData[i].email_len + pData[i].name_len + pData[i].relation_len;
		}
	}
	~MSG_RESP_GETFRIENDLIST()
	{
		SAFE_DEL_ARRAY(pData);
	}

	const char* GetBuffer()
	{
		char* pBuf = new char[nSize];
		size_t offset = sizeof(MSG_HEADER) + sizeof(int);
		memcpy(pBuf, this, sizeof(char) * offset);
		// 헤더 ~ nCnt까지 삽입

		for (int i = 0; i < nCnt; i++)
		{
			size_t data_size = 31 + pData[i].email_len + pData[i].name_len + pData[i].relation_len;
			memcpy(&pBuf[offset], &pData[i], data_size);	// 데이터 크기만큼 데이터 삽입
			offset += data_size;	// 삽입한 데이터 크기만큼 오프셋 증가
		}

		return pBuf;
	}
};		

//////////////////////////////////////////////////////////////////////////
// CTS 메시지
//////////////////////////////////////////////////////////////////////////

struct MSG_CTS_GPSDATA : public MSG_HEADER		// GPS 데이터
{
	double		latitude;
	double		longitude;
	int			speed;
	int			bearing;
	int			accuracy;
	DATETIME_STRING	datetime;

	MSG_CTS_GPSDATA()
		: MSG_HEADER(CTS_GPSDATA, sizeof(MSG_CTS_GPSDATA))
	{
		memset(time, 0, sizeof(char) * 19);
	}
};
struct MSG_CTS_REGID : public MSG_HEADER
{
	int regid_len;
	char buf[MAX_REGID_LEN];

	MSG_CTS_REGID()
		: MSG_HEADER(CTS_REGID)
	{
		memset(buf, 0, sizeof(char) * MAX_REGID_LEN);
	}
};
struct MSG_CTS_ECALL : public MSG_HEADER	// 긴급호출
{
	MSG_CTS_ECALL()
		: MSG_HEADER(CTS_ECALL, sizeof(MSG_CTS_ECALL))
	{}
};

#pragma pack(pop)