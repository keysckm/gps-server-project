#include "stdafx.h"
#include "Log.h"
#pragma warning(disable:4996)
Log::Log(void)
{
}


Log::~Log(void)
{
}

void Log::__write(string& _rLog, ErrorType _type, bool _IsWriteFile /* = true */)
{
	static std::mutex printout_mutex;
	static std::mutex fileout_mutex;

	SYSTEMTIME	sysTime;	// system time
	TCHAR		CurrentTime[32]	= {'\0', };				// 시간 문자열
	string		strFullLog;	// 최종적으로 출력될 로그

	::GetLocalTime(&sysTime);	// systemtime 가져옴
	_sntprintf(CurrentTime, 32, _T("[%d-%02d-%02d %02d:%02d:%02d]"),
		sysTime.wYear,
		sysTime.wMonth,
		sysTime.wDay,
		sysTime.wHour,
		sysTime.wMinute,
		sysTime.wSecond);

	// 로그 출력 일자/시각 폰트 색상 설정
	if(_type == ET_I || _type == ET_V)
		SET_CONSOLE_COLOR(CC_WHITE);
	else
	{
		switch(_type)
		{
		case ET_E:	SET_CONSOLE_COLOR(CC_RED_BG);		break;
		case ET_W:	SET_CONSOLE_COLOR(CC_YELLOW_BG);	break;
		case ET_D:	SET_CONSOLE_COLOR(CC_WHITE_BG);		break;
		}
	}

	// 로그 출력 일자/시각 출력
	cout<<CurrentTime;
	
	int nColor = CC_WHITE;
	switch(_type)
	{
	case ET_I:	nColor = CC_GREEN;				break;
	case ET_E:	nColor = CC_RED;				break;
	case ET_W:	nColor = CC_YELLOW;				break;
	case ET_D:	nColor = CC_WHITE_BG | CC_RED;	break;
	case ET_V:	nColor = CC_SKYBLUE;			break;
	}

	printout_mutex.lock();
	SET_CONSOLE_COLOR(nColor);

	cout<<"["<<static_cast<char>(_type)<<"] "<<_rLog<<"\n";
	printout_mutex.unlock();
	//printf("[%c] %s\n", static_cast<char>(_type), _rLog.c_str());

	if(_IsWriteFile)
	{
		TCHAR		szLogFilePath[MAX_PATH] = {'\0', };		// 로그파일 경로
		GetLogPath(szLogFilePath, MAX_PATH);				// 로그파일 경로 획득

		FILE* f = _tfopen(szLogFilePath, "a");				// 추가 모드로 파일 열기
		if(!f)	// 파일 열기 실패
		{		// 콘솔로 에러 출력
			auto nErr = GetLastError();
			string strMsg = _T("Write Failed!!!!!!!\n");
			strMsg += Log::GetSysErrMsg(nErr);
			
			__write(strMsg, ET_E, false);

			return;
		}

		// 로그 데이터 누적
		strFullLog += CurrentTime;	// 로그 발생 시각 누적
		strFullLog += "[";
		strFullLog += static_cast<char>(_type);
		strFullLog += "] ";
		strFullLog += _rLog;		// 로그 데이터 누적
		strFullLog += "\n";			// 줄바꿈
		fileout_mutex.lock();
		_ftprintf(f, _T("%s"), strFullLog.data());
		fileout_mutex.unlock();
		fclose(f);
	}
}

void Log::GetLogPath(_OUT_ TCHAR* _pszBuf, int _nBufSize)
{
	SAFE_PTR(_pszBuf);

	TCHAR szFullpath[MAX_PATH] = {'\0', };				// 경로
	::GetModuleFileName(NULL, szFullpath, MAX_PATH);	// 가져옴
	SYSTEMTIME sysTime;

	::GetLocalTime(&sysTime);	// systemtime 가져옴

	_sntprintf(_pszBuf, MAX_PATH, _T("%s.%d-%02d-%02d.log"),	// log파일이 실제 저장될 경로
		szFullpath,
		sysTime.wYear,
		sysTime.wMonth,
		sysTime.wDay);
}

void Log::e(const TCHAR* _pszLog, ...)
{
	SAFE_PTR(_pszLog);

	va_list		ap;		// 동적 매게 변수
	TCHAR		szBuf[MAX_LOG_BUF];

	va_start(ap, _pszLog);	// 가변인자 획득
	_vstprintf(szBuf, _pszLog, ap);	// 인자로부터 문자열 추출
	va_end(ap);				// 가변인자 종료

	string strLog;
	strLog += szBuf;

	__write(strLog, ET_E);
}

void Log::w(const TCHAR* _pszLog, ...)
{
	SAFE_PTR(_pszLog);

	va_list		ap;		// 동적 매게 변수
	TCHAR		szBuf[MAX_LOG_BUF];

	va_start(ap, _pszLog);	// 가변인자 획득
	_vstprintf(szBuf, _pszLog, ap);	// 인자로부터 문자열 추출
	va_end(ap);				// 가변인자 종료

	string strLog;
	strLog += szBuf;

	__write(strLog, ET_W);
}

void Log::i(const TCHAR* _pszLog, ...)
{
	SAFE_PTR(_pszLog);

	va_list		ap;		// 동적 매게 변수
	TCHAR		szBuf[MAX_LOG_BUF];

	va_start(ap, _pszLog);	// 가변인자 획득
	_vstprintf(szBuf, _pszLog, ap);	// 인자로부터 문자열 추출
	va_end(ap);				// 가변인자 종료

	string strLog;
	strLog += szBuf;

	__write(strLog, ET_I);
}

void Log::d(const TCHAR* _pszLog, ...)
{
	SAFE_PTR(_pszLog);

	va_list		ap;		// 동적 매게 변수
	TCHAR		szBuf[MAX_LOG_BUF];

	va_start(ap, _pszLog);	// 가변인자 획득
	_vstprintf(szBuf, _pszLog, ap);	// 인자로부터 문자열 추출
	va_end(ap);				// 가변인자 종료

	string strLog;
	strLog += szBuf;

	__write(strLog, ET_D);
}

void Log::v(const TCHAR* _pszLog, ...)
{
	SAFE_PTR(_pszLog);

	va_list		ap;		// 동적 매게 변수
	TCHAR		szBuf[MAX_LOG_BUF];

	va_start(ap, _pszLog);	// 가변인자 획득
	_vstprintf(szBuf, _pszLog, ap);	// 인자로부터 문자열 추출
	va_end(ap);				// 가변인자 종료

	string strLog;
	strLog += szBuf;

	SET_CONSOLE_COLOR(CC_GREEN_BG);
	__write(strLog, ET_V);
}

// 시스템 에러를 출력
string Log::GetSysErrMsg(int _nErrNo)
{
	LPVOID pMsg = nullptr;

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, _nErrNo, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&pMsg, 0, NULL);

	string strMsg((LPCTSTR)pMsg);
	LocalFree(pMsg);

	return strMsg;
}