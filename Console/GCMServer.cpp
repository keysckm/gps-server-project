#include "stdafx.h"
#include "GCMServer.h"
#include <wininet.h>

#pragma comment(lib, "wininet.lib")

const std::string GCMServer::m_strProjectId = "511922226334";
const std::string GCMServer::m_strApiKey = "AIzaSyB6UIIw_iVnq_eCqwGYi5XvwyGR3OWScUg";

GCMServer::GCMServer()
{
}


GCMServer::~GCMServer()
{
}

#pragma warning(disable:4996)

void GCMServer::URIEncode(_IN_ const char* _pszBuf, _OUT_ char** _pszOut)
{
	SAFE_PTR(_pszBuf);

	char* pszBuf = new char[(strlen(_pszBuf) * 3) + 1];
	SAFE_PTR(pszBuf);

	int i, j;
	for (i = j = 0; _pszBuf[i]; i++)
	{
		unsigned char c = (unsigned char) _pszBuf[i];
		if ((c >= '0') && (c <= '9')) pszBuf[j++] = c;
		else if (c == ' ') pszBuf[j++] = '+';    // 웹에 맞게 추가
		else if ((c >= 'A') && (c <= 'Z')) pszBuf[j++] = c;
		else if ((c >= 'a') && (c <= 'z')) pszBuf[j++] = c;
		else if ((c == '@') || (c == '.') || (c == '/') || (c == '\\')
			|| (c == '-') || (c == '_') || (c == ':'))
			pszBuf[j++] = c;
		else
		{
			char buf[2 + 1] = { '\0', };
			sprintf(buf, "%02x", c);
			pszBuf[j++] = '%';
			pszBuf[j++] = buf[0];
			pszBuf[j++] = buf[1];
		}
	}
	pszBuf[j] = '\0';

	*_pszOut = pszBuf;
}

bool GCMServer::Push(const char* _pszRegid, const std::vector<std::tuple<std::string, std::string>>& _rItems)
{
	SAFE_PTR(_pszRegid) false;

	HANDLE hConnect = NULL;
	HANDLE hHttp = NULL;
	HANDLE hReq = NULL;
	std::string strResult;

	try
	{
		// URL 오픈
		hConnect = ::InternetOpen("GCM", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
		if (hConnect == NULL)
			throw "InternetOpen";

		// GCM 서버 접속
		hHttp = ::InternetConnect(hConnect, "android.googleapis.com", INTERNET_DEFAULT_HTTPS_PORT, NULL, NULL, INTERNET_SERVICE_HTTP, 0, NULL);
		if (hHttp == NULL)
			throw "InternetConnect";

		// Post Request
		hReq = HttpOpenRequest(hHttp, "POST", "/gcm/send", "HTTP/1.1", NULL, NULL,
			INTERNET_FLAG_NO_COOKIES | INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID, 0);
		if (hReq == NULL)
			throw "HttpOpenRequest";

		// HTTP request 헤더
		std::stringstream s;
		s << "Content-Type:application/x-www-form-urlencoded;charset=UTF-8\r\n";
		s << "Authorization:key=" << m_strApiKey;
		s << "\r\n\r\n";
		std::string strHeader = s.str();

		if (!HttpAddRequestHeaders(hReq, strHeader.c_str(), strHeader.length(), HTTP_ADDREQ_FLAG_REPLACE | HTTP_ADDREQ_FLAG_ADD))
			throw "HttpAddRequestHeaders";

		std::stringstream msgbuf;
		msgbuf << "registration_id=" << _pszRegid;

		// request body
		for (std::tuple<std::string, std::string> t : _rItems)
		{
			const char* strDataTag = std::get<0>(t).c_str();	// tuple<0> = data.xxx
			const char* strDataMsg = std::get<1>(t).c_str();	// tuple<1> = message

			// MultiByte -> Unicode (16)
			int nLen = MultiByteToWideChar(CP_ACP, 0, strDataMsg, strlen(strDataMsg), NULL, NULL);
			std::wstring strUnicode(nLen, '\0');
			MultiByteToWideChar(CP_ACP, 0, strDataMsg, strlen(strDataMsg), &strUnicode[0], nLen);

			// Unicode (16) -> Unicode(8)
			nLen = WideCharToMultiByte(CP_UTF8, 0, strUnicode.c_str(), strUnicode.length(), NULL, 0, NULL, NULL);
			char* pszUtf8 = new char[nLen + 1];
			WideCharToMultiByte(CP_UTF8, 0, strUnicode.c_str(), strUnicode.length(), pszUtf8, nLen, NULL, NULL);
			pszUtf8[nLen] = '\0';

			// Unicode (8) -> URI Encode
			char* pszUristring = nullptr;
			URIEncode(pszUtf8, &pszUristring);
			if (!pszUristring)
			{
				SAFE_DEL_ARRAY(pszUtf8);
				throw "URIEncode";
			}

			msgbuf << "&data." << strDataTag << "=" << pszUristring;
			SAFE_DEL_ARRAY(pszUtf8);
			SAFE_DEL_ARRAY(pszUristring);
		}

		std::string strMsg = msgbuf.str();

		if (!HttpSendRequest(hReq, NULL, 0, (LPVOID) strMsg.c_str(), strMsg.length()))
			throw HttpSendRequest;

		DWORD dwStateCode;
		DWORD dwStateCodeLen;
		::HttpQueryInfo(hReq, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &dwStateCode, &dwStateCodeLen, NULL);
		if (dwStateCode != 200)
			throw "Resp State Code is Not 200";

		char szBuffer[1024] = { '\0', };
		DWORD dwRead;
		std::stringstream strResultStream;
		while (::InternetReadFile(hReq, szBuffer, 1024, &dwRead) && dwRead > 0)
		{
			szBuffer[dwRead] = '\0';
			strResultStream << szBuffer;
		}

		strResult = strResultStream.str();

		Log::i("GCM Push 성공");
		Log::v("GCM으로부터: %s", strResult.c_str());
	}
	catch (std::string e)
	{
		Log::e("GCM Push 실패");
		Log::v(e.c_str());
	}

	SAFE_CLOSE_INTERNET_HANDLE(hReq);
	SAFE_CLOSE_INTERNET_HANDLE(hConnect);
	SAFE_CLOSE_INTERNET_HANDLE(hHttp);

	return true;
}

bool GCMServer::Push(const char* _pszRegid, std::string _strTag, std::string _strData)
{
	SAFE_PTR(_pszRegid) false;

	HANDLE hConnect = NULL;
	HANDLE hHttp = NULL;
	HANDLE hReq = NULL;
	std::string strResult;

	try
	{
		// URL 오픈
		hConnect = ::InternetOpen("GCM", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
		if (hConnect == NULL)
			throw "InternetOpen";

		// GCM 서버 접속
		hHttp = ::InternetConnect(hConnect, "android.googleapis.com", INTERNET_DEFAULT_HTTPS_PORT, NULL, NULL, INTERNET_SERVICE_HTTP, 0, NULL);
		if (hHttp == NULL)
			throw "InternetConnect";

		// Post Request
		hReq = HttpOpenRequest(hHttp, "POST", "/gcm/send", "HTTP/1.1", NULL, NULL,
			INTERNET_FLAG_NO_COOKIES | INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID, 0);
		if (hReq == NULL)
			throw "HttpOpenRequest";

		// HTTP request 헤더
		std::stringstream s;
		s << "Content-Type:application/x-www-form-urlencoded;charset=UTF-8\r\n";
		s << "Authorization:key=" << m_strApiKey;
		s << "\r\n\r\n";
		std::string strHeader = s.str();

		if (!HttpAddRequestHeaders(hReq, strHeader.c_str(), strHeader.length(), HTTP_ADDREQ_FLAG_REPLACE | HTTP_ADDREQ_FLAG_ADD))
			throw "HttpAddRequestHeaders";

		std::stringstream msgbuf;
		msgbuf << "registration_id=" << _pszRegid;

		// request body
		const char* strDataTag = _strTag.c_str();	// tuple<0> = data.xxx
		const char* strDataMsg = _strData.c_str();	// tuple<1> = message

		// MultiByte -> Unicode (16)
		int nLen = MultiByteToWideChar(CP_ACP, 0, strDataMsg, strlen(strDataMsg), NULL, NULL);
		std::wstring strUnicode(nLen, '\0');
		MultiByteToWideChar(CP_ACP, 0, strDataMsg, strlen(strDataMsg), &strUnicode[0], nLen);

		// Unicode (16) -> Unicode(8)
		nLen = WideCharToMultiByte(CP_UTF8, 0, strUnicode.c_str(), strUnicode.length(), NULL, 0, NULL, NULL);
		char* pszUtf8 = new char[nLen + 1];
		WideCharToMultiByte(CP_UTF8, 0, strUnicode.c_str(), strUnicode.length(), pszUtf8, nLen, NULL, NULL);
		pszUtf8[nLen] = '\0';

		// Unicode (8) -> URI Encode
		char* pszUristring = nullptr;
		URIEncode(pszUtf8, &pszUristring);
		if (!pszUristring)
		{
			SAFE_DEL_ARRAY(pszUtf8);
			throw "URIEncode";
		}

		msgbuf << "&data." << strDataTag << "=" << pszUristring;
		SAFE_DEL_ARRAY(pszUtf8);
		SAFE_DEL_ARRAY(pszUristring);

		std::string strMsg = msgbuf.str();

		if (!HttpSendRequest(hReq, NULL, 0, (LPVOID) strMsg.c_str(), strMsg.length()))
			throw HttpSendRequest;

		DWORD dwStateCode;
		DWORD dwStateCodeLen;
		::HttpQueryInfo(hReq, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &dwStateCode, &dwStateCodeLen, NULL);
		if (dwStateCode != 200)
			throw "Resp State Code is Not 200";

		char szBuffer[1024] = { '\0', };
		DWORD dwRead;
		std::stringstream strResultStream;
		while (::InternetReadFile(hReq, szBuffer, 1024, &dwRead) && dwRead > 0)
		{
			szBuffer[dwRead] = '\0';
			strResultStream << szBuffer;
		}

		strResult = strResultStream.str();

		Log::i("GCM Push 성공");
		Log::v("GCM으로부터: %s", strResult.c_str());
	}
	catch (std::string e)
	{
		Log::e("GCM Push 실패");
		Log::v(e.c_str());
	}

	SAFE_CLOSE_INTERNET_HANDLE(hReq);
	SAFE_CLOSE_INTERNET_HANDLE(hConnect);
	SAFE_CLOSE_INTERNET_HANDLE(hHttp);

	return true;
}