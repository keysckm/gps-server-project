#ifndef __GPSDATA_H__
#define __GPSDATA_H__
#include "stdafx.h"
#pragma pack(push, 1)

struct LOCATIONDATA
{
	TOKEN	user;		// 이 위치정보의 대상(유저 토큰)
	double	latitude;
	double	longitude;
	int		speed;
	int		bearing;
	int		accuracy;
	char	date[19];

	LOCATIONDATA()
		: user(0)
		, latitude(.0)
		, longitude(.0)
		, speed(0)
		, bearing(0)
		, accuracy(0)
	{
		memset(date, 0, sizeof(char) * 19);
	}

	LOCATIONDATA(const char* _user, const char* _latitude, const char* _longitude, const char* _speed, const char* _bearing, const char* _accuracy, const char* _date)
	{
		user = _atoi64(_user);
		latitude = atof(_latitude);
		longitude = atof(_longitude);
		speed = atoi(_speed);
		bearing = atoi(_bearing);
		accuracy = atoi(_accuracy);
		memcpy(date, _date, sizeof(char) * 19);
	};

	LOCATIONDATA(const LOCATIONDATA& _ref)
	{
		user = _ref.user;
		latitude = _ref.latitude;
		longitude = _ref.longitude;
		speed = _ref.speed;
		bearing = _ref.bearing;
		accuracy = _ref.accuracy;
		memcpy(date, _ref.date, sizeof(char) * 19);
	}

	void operator = (const LOCATIONDATA& _ref)
	{
		user = _ref.user;
		latitude = _ref.latitude;
		longitude = _ref.longitude;
		speed = _ref.speed;
		bearing = _ref.bearing;
		accuracy = _ref.accuracy;
		memcpy(date, _ref.date, sizeof(char) * 19);
	}
};

#pragma pack(pop)
#endif