#pragma once

class ClientSession
{
private:
	TOKEN	m_Device;
	TOKEN	m_User;
	char	m_szId[MAX_ID_LEN + 1];			// 아이디
	char	m_szRegid[MAX_REGID_LEN + 1];	// regid

public:
	ClientSession()
		: m_Device(0)
		, m_User(0)
	{
		memset(m_szRegid, 0, sizeof(char) * (MAX_REGID_LEN + 1));
		memset(m_szId, 0, sizeof(char) * (MAX_ID_LEN + 1));	
	}

	TOKEN GetDevice() const { return m_Device; }
	void SetDevice(TOKEN val) { m_Device = val; }
	TOKEN GetUser() const { return m_User; }
	void SetUser(TOKEN val) { m_User = val; }
	const char* GetId() const { return m_szId; }
	void SetId(const char* _pszId)
	{
		memcpy(m_szId, _pszId, MAX_ID_LEN);
	}
	const char* GetRegId() const { return m_szRegid; }
	void SetRegId(const char* _pszRegId, size_t _nRegid_len)
	{
		memcpy(m_szRegid, _pszRegId, _nRegid_len);
	}
	void Clear()
	{
		m_Device = m_User = 0;
		memset(m_szId, 0, sizeof(char) * (MAX_ID_LEN + 1));
		memset(m_szRegid, 0, sizeof(char) * MAX_REGID_LEN + 1);
	}
};

class Session
{
private:
	boost::asio::ip::tcp::socket		m_Socket;		// socket
	std::array<char, MAX_RECV_BUF_LEN>	m_RecvBuf;		// Receive buffer
	std::array<char, MAX_RECV_BUF_LEN>	m_PackBuf;		// Packet Buffer
	size_t								m_nPackMark;	// 지금까지 받은 패킷 사이즈
	std::deque<char*>					m_pSendQ;		// 보낼 데이터 큐
	bool								m_isSend;		// 현재 보내는중
	std::mutex							m_QMutex;		// 보낼 데이터 큐 뮤텍스

	boost::asio::io_service::strand		m_strand;	// stramd객체를 통한 동기화

	// 디바이스 정보
	ClientSession	m_SessionData;

private:
	// 비동기 수신함수에 대한 핸들링 함수
	__callback	void __handle_recv(const boost::system::error_code& _rErr, const size_t _nBytes_transferred);
	// 비동기 송신함수에 대한 핸들링 함수
	__callback	void __handle_send(const boost::system::error_code& _rErr, const size_t _nBytes_transferred);

public:
	Session(boost::asio::io_service& _rIoServ);
	~Session(void);

	// 초기화
	void Init();
	// 종료
	void Stop();

	// IP주소
	std::string GetIP()
	{
		return m_Socket.remote_endpoint().address().to_string();
	}

	ClientSession& GetClient()
	{
		return m_SessionData;
	}

	// 수신시작
	void PostRecv();
	// 송신시작
	void PostSend(const char* _pSendBuf, size_t _nSendSize);
	
	inline boost::asio::ip::tcp::socket& Socket()
	{
		return m_Socket;
	}

	inline ClientSession& GetSessionData()
	{
		return m_SessionData;
	}
};

