#pragma once
class Connection
{
private:
	std::mutex		m_lock;

	boost::asio::ip::udp::socket		m_sock;	// 클라이언트 소켓
	boost::asio::ip::udp::endpoint		m_ep;	// 클라이언트 접속지

	std::array<char, MAX_RECV_BUF_LEN>		m_RecvBuf;	// 수신 버퍼
	std::deque<std::tuple<char*, size_t>>	m_SendQueue;	// 송신 버퍼
	bool m_IsSend;		// 송신 플래그
private:
	void __handle_recv(const boost::system::error_code& _rErr, const size_t _nBytes_transferred);
	void __handle_send(const boost::system::error_code& _rErr, const size_t _nBytes_transferred);

public:
	Connection(boost::asio::io_service& _rIo);
	~Connection();

	void PostReceive();
	void PostSend(const char* _pSendBuf, size_t _nSendSize, bool _IsContinue = false);
};

