#include "stdafx.h"
#include "Token.h"

#include <chrono>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>

TOKEN TokenGenerator::Generate()
{
	chrono::system_clock::time_point tp = chrono::system_clock::now();
	__int64 t = static_cast<__int64>(chrono::system_clock::to_time_t(tp));

	// 0x0~0xF값을 랜덤하게 뽑아내는 제네레이터 준비
	boost::random::mt19937 gen((unsigned) time(NULL));
	boost::random::uniform_int_distribution<> RandGen(0x0, 0xF);

	// t를 왼쪽으로 4bit shift
	t <<= 4;
	// 값 합성
	t |= RandGen(gen);

	return static_cast<TOKEN>(t);
}