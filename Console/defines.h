#ifndef __DEFINES_H__
#define __DEFINES_H__

/************************************************************************
 * defines.h
 * 작성일: 12. 05. 20, 15:27
 *
 * 매크로 모음
 ************************************************************************
 * 13. 05. 20, 15:27.
 *	최초 작성 시작
 * 13. 10. 21.
 *	safe 체크 매크로 정리
 * 13. 11. 02.
 *	콘솔창의 텍스트 색상을 변경하는 매크로 설정
 * 13. 11. 11.
 *	콜백함수임을 알리는 _CALLBACK_ 매크로 설정
 ************************************************************************/
						

// INPUT/OUTPUT PARAM
#define _IN_
#define _OUT_

// OPTION PARAM
#define _OPT_

// CALLBACK FUNCTION
#define _CALLBACK_

// Safe Delete
#define SAFE_DEL(ptr)		if(ptr)\
								delete ptr;\
							ptr = nullptr
#define SAFE_DEL_ARRAY(ptr)	if(ptr)\
								delete []ptr;\
							ptr = nullptr

// ptr의 nullptr 체크 리턴
#define SAFE_PTR(ptr)	if(!ptr) return
#define SAFE_CONDI(ptr)	if(ptr) return

// Safe Release, COM Objecct Only
#define SAFE_RELEASE(ptr)	if(ptr)\
	ptr->Release();\
	ptr = nullptr

// SAFE Close Handle, Handle Object Only
#define SAFE_CLOSE_INTERNET_HANDLE(h) if(h)\
	InternetCloseHandle(h);\
	h = NULL

// D2D Factory 생성
#define CREATE_D2D_SINGLE_THREADED(_ipD2DFtry)	::D2D1CreateFactory(\
													D2D1_FACTORY_TYPE::D2D1_FACTORY_TYPE_SINGLE_THREADED,\
													&_ipD2DFtry)
#define CREATE_D2D_MULTI_THREADED(_ipD2DFtry)	::D2D1CreateFactory(\
													D2D1_FACTORY_TYPE::D2D1_FACTORY_TYPE_MULTI_THREADED,\
													&_ipD2DFtry)
// WIC Factory 생성
#define CREATE_WIC_FACTORY(_ipWICFtry)			::CoCreateInstance(\
													CLSID_WICImagingFactory,\
													NULL,\
													CLSCTX_INPROC_SERVER,\
													IID_PPV_ARGS(&_ipWICFtry))


// 콘솔 글자 색상 매크로
// SetConsoleTextAttribute 함수 이용

#define SET_CONSOLE_COLOR(color)	::SetConsoleTextAttribute(::GetStdHandle(STD_OUTPUT_HANDLE), color)

#define CC_RED         (FOREGROUND_RED | FOREGROUND_INTENSITY)
#define CC_GREEN       (FOREGROUND_GREEN | FOREGROUND_INTENSITY)
#define CC_BLUE        (FOREGROUND_BLUE | FOREGROUND_INTENSITY)
#define CC_YELLOW      (CC_RED | CC_GREEN)
#define CC_PINK        (CC_RED | CC_BLUE)
#define CC_SKYBLUE     (CC_GREEN | CC_BLUE)
#define CC_WHITE       (CC_RED | CC_GREEN | CC_BLUE)

#define CC_RED_BG      (BACKGROUND_RED | BACKGROUND_INTENSITY)
#define CC_GREEN_BG    (BACKGROUND_GREEN | BACKGROUND_INTENSITY)
#define CC_BLUE_BG     (BACKGROUND_BLUE | BACKGROUND_INTENSITY)
#define CC_YELLOW_BG   (CC_RED_BG | CC_GREEN_BG)
#define CC_PINK_BG     (CC_RED_BG | CC_BLUE_BG)
#define CC_SKYBLUE_BG  (CC_GREEN_BG | CC_BLUE_BG)
#define CC_WHITE_BG    (CC_RED_BG | CC_GREEN_BG | CC_BLUE_BG)

#endif