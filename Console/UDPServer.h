#pragma once
class UDPServer
{
private:
	boost::asio::io_service		m_ioserv;	// io service

	std::vector<Connection*>	m_ConnectionList;	// 커넥션 리스트
	std::vector<boost::thread*>	m_ThreadList;		// 스레드 리스트
	
	UDPServer();
	~UDPServer();
	UDPServer(UDPServer& _rSrc){}
	UDPServer operator = (UDPServer& _rSrc){}

public:
	static UDPServer& GetServer();

	void Run();	// 서버 시작

	void AddConnection(size_t _nSize);	// size만큼 커넥션 추가
	void AddIoRun(size_t _nSize);		// size만큼 스레드 추가

	static bool IsValidToken(TOKEN _Device, TOKEN _User);
	// 메시지 처리기
	static void ProcessPacket(Connection* _pConn, const MSG_HEADER* _pMsg);
};

