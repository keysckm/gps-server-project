#include "stdafx.h"
#include "Server.h"
#include <boost/date_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>
#include <iomanip>

boost::asio::io_service g_ioserv;

Server::Server()
	: m_Acceptor(m_ioserv,
		boost::asio::ip::tcp::endpoint(
			boost::asio::ip::tcp::v4(), SERVER_PORT))
{
}

Server::~Server(void)
{
}

void Server::Run()
{
	Log::i(_T(">>>>>>>>>> Server Running... <<<<<<<<<<"));
	Log::v(_T("Port no: %d"), SERVER_PORT);

	// 데이터베이스에 연결
	if (!MariaDB::Connect())
		return;
	
	// Accept
	__post_accept();

	// ioservice run thread
	for (int i = 0; i < 10; i++)
	{
		boost::thread* pThread = new boost::thread(
			boost::bind(&boost::asio::io_service::run,
			&m_ioserv)
			);

		m_ThreadList.push_back(pThread);
	}

	// thread join...
	for (boost::thread* pThread : m_ThreadList)
	{
		pThread->join();
		SAFE_DEL(pThread);
	}
}

void Server::__post_accept()
{
	Session* pNewSession = new Session(m_ioserv);

	m_Acceptor.async_accept(pNewSession->Socket(),
		boost::bind(&Server::__handle_accept, this,
		pNewSession, boost::asio::placeholders::error)
		);
}

void Server::__handle_accept(Session* _pSession, const boost::system::error_code& _rErr)
{
	if (!_rErr)
	{
		// 세션을 관리 리스트에 삽입한다.
		m_SessionList.push_back(_pSession);

		Log::i(_T("Connected Client"));
		Log::v(_T("from: %s"), _pSession->GetIP().c_str());

		// 세션초기화
		_pSession->Init();
	}
	else
	{
		SAFE_DEL(_pSession);	// 접속실패, 세션파기
		Log::e(_T("Connect Failed"));
		Log::v(_T("ErrNo: %d, ErrMsg: %s"), _rErr.value(), _rErr.message());
	}

	// Accept 시작
	__post_accept();
}

void Server::CloseSession(Session* _pSession)
{
	SAFE_PTR(_pSession);

	std::string strAddress = _pSession->GetIP();

	// 세션리스트에서 세션 제거
	m_SessionList.remove(_pSession);

	// 세션 파기
	SAFE_DEL(_pSession);

	Log::i(_T("Shutdown Session: %s"), strAddress.c_str());
}

// 서버에러
#define RESP_RESULT_SERVERERR(REQUEST)		{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::SERVERERR); \
	_pSession->PostSend((char*) &resp, resp.nSize); break;}
// 실패
#define RESP_RESULT_REFUSAL(REQUEST)		{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::REFUSAL); \
	_pSession->PostSend((char*) &resp, resp.nSize); break;}
// 성공
#define RESP_RESULT_APPROVED(REQUEST)		{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::APPROVED); \
	_pSession->PostSend((char*) &resp, resp.nSize); break;}
// 무효한 토큰
#define RESP_RESULT_INVALIDTOKNEN(REQUEST)	{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::INVALIDTOKEN); \
	_pSession->PostSend((char*) &resp, resp.nSize); break;}
// 기타
#define RESP_RESULT(REQUEST, RESULT)		{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::RESULT); \
	_pSession->PostSend((char*) &resp, resp.nSize); break;}

// stringstream 클리어
#define CLEAR_SS(stringstream) stringstream.str(std::string()); stringstream.clear()	

// 토큰 검사
#define CHK_TOKEN(REQUEST){if (!IsValidToken(pMsg->nDevice, pMsg->nUser))\
		{\
			RESP_RESULT_INVALIDTOKNEN(REQUEST);\
			break;\
		}}

// 결과가 없는 쿼리날리기, CLEAR_SS도 알아서 해준다.
#define SEND_QUERY(stringstream) {if (!MariaDB::SendQuery(stringstream)){ \
									RESP_RESULT_SERVERERR(_pMsg->nIdx);\
									break;}\
									CLEAR_SS(stringstream);}

#define SEND_QUERY_RESULT(stringstream, queryresult){if (!MariaDB::SendQuery(stringstream, queryresult)){ \
									RESP_RESULT_SERVERERR(_pMsg->nIdx);\
									break;}\
									CLEAR_SS(stringstream);}

bool Server::IsValidToken(TOKEN _Device, TOKEN _User)
{
	std::stringstream s;
	QueryResult result;

	s << "SELECT ID FROM MEMBERSHIPS WHERE IDX='" << _User << "' AND DEVICETOKEN='" << _Device << "';";
	if (!MariaDB::SendQuery(s, result))
		return false;
	if (result.IsEmpty())
	{	// 토큰이 유효하지 않음
		Log::w(_T("Invalid Token"));
		Log::v(_T("Device: %d, User: %d"), _Device, _User);
		return false;
	}

	return true;
}

 void Server::ProcessPacket(Session* _pSession, const MSG_HEADER* _pMsg)
 {
	 SAFE_PTR(_pSession);
	 SAFE_PTR(_pMsg);

	 QueryResult result;		// 쿼리 결과
	 std::stringstream s;	// 문자열 스트림

	 Log::i(_T("MessageType: %d"), _pMsg->nIdx);

	 switch (_pMsg->nIdx)
	 {
#pragma region REQ_ACCESS
	 case REQ_ACCESS:
	 {
		 const MSG_REQ_ACCESS* pMsg = static_cast<const MSG_REQ_ACCESS*>(_pMsg);
		 if (pMsg->nDevice == INVALID_TOKEN)	// 디바이스 토큰이 무효값이다. -> 첫 접속
		 {
			 TOKEN device = TokenGenerator::Generate();		// 토큰값 제네레이트
			 std::string date = Server::NowDateTimeString();	// 쿼리
			 s << "INSERT INTO DEVICES(DEVICE_IDX, LASTACCESS) VALUES('" << device << "', '" <<
				 date.c_str() << "');";

			 SEND_QUERY(s);

			 _pSession->GetClient().SetDevice(device);
			 MSG_RESP_DEVICETOKEN msg(device);			// 토큰 송신
			 _pSession->PostSend((char*) &msg, msg.nSize);
			 break;
		 }
		 else if (pMsg->nUser == INVALID_TOKEN)	// 디바이스는 접속한적이 있는데 로그인은 한적이 없음 -> 로그인페이지로 가야함
		 {
			 _pSession->GetClient().SetDevice(pMsg->nDevice);
			 s << "UPDATE DEVICES SET LASTACCESS='" << Server::NowDateTimeString() << "';";	// 일단 디바이스는 접속함
			 RESP_RESULT_APPROVED(CTS_REGID);	// CTS_REGID의 APPROVED가 로그인화면으로 가도록 되어있음. 일단 임시로 CTS_REGID를 사용
			 break;
		 }
		 else if (IsValidToken(pMsg->nDevice, pMsg->nUser))	// 디바이스 토큰과 유저 토큰이 유효함
		 {
			 // 바로 접속 허가
			 s << "UPDATE DEVICES SET LASTACCESS='" << Server::NowDateTimeString() << "';";
			 SEND_QUERY(s);

			 _pSession->GetClient().SetDevice(pMsg->nDevice);
			 _pSession->GetClient().SetUser(pMsg->nUser);
			 
			 RESP_RESULT_APPROVED(REQ_ACCESS);
			 break;
		 }
		 else
		 {
			 // 여기까지 왔으면 이제 막장이죠. 토큰이 유효하지않아욧!
			 RESP_RESULT(REQ_ACCESS, INVALIDTOKEN);
			 break;
		 }
		 break;
	 }	// case REQ_ACCESS
#pragma endregion
#pragma region REQ_SIGNUP
	 case REQ_SIGNUP:
	 {
		 const MSG_REQ_SIGNUP* pSignUp = static_cast<const MSG_REQ_SIGNUP*>(_pMsg);

		 char szId[MAX_ID_LEN + 1] = { '\0', };
		 char szPw[MAX_PW_LEN + 1] = { '\0', };
		 char szName[MAX_NAME_LEN + 1] = { '\0', };

		 // 메시지로부터 ID와 PW데이터를 추출한다.
		 memcpy(szPw, pSignUp->Password, sizeof(char) * MAX_PW_LEN);	// pw 카피
		 memcpy(szId, pSignUp->buf, sizeof(char) * pSignUp->id_len);	// id 카피
		 memcpy(szName, &pSignUp->buf[pSignUp->id_len], sizeof(char) * pSignUp->name_len);	// name 카피

		 // id 중복 검사
		 s << "SELECT IDX FROM MEMBERSHIPS WHERE ID='" << szId << "';";
		 SEND_QUERY_RESULT(s, result);
		 if (!result.IsEmpty())
		 {
			 // 중복 ID 있음
			 RESP_RESULT_REFUSAL(REQ_SIGNUP);

			 Log::i(_T("SignUp Failed"));
			 Log::v(_T("ID is Exist"));
			 break;
		 }

		 TOKEN user = TokenGenerator::Generate();
		 std::string datetime_string = Server::NowDateTimeString();
		 // ID 중복 없음
		
		 s << "INSERT INTO MEMBERSHIPS (IDX, ID, PW, NAME, CREATEDATE, PWCHANGEDATE) VALUES('" << user << "', '" << szId << "', '" << szPw << "', '" <<
			 szName << "', '" << datetime_string << "', '" << datetime_string << "');";
		 SEND_QUERY(s);

		 RESP_RESULT_APPROVED(REQ_SIGNUP);
		 Log::i(_T("SignUp Successed"));
		 Log::v(_T("id: %s"), szId);
		 break;
	 }	// case REQ_SIGNUP
#pragma endregion
#pragma region REQ_LOGIN
	 case REQ_LOGIN:
	 {
		 const MSG_REQ_LOGIN* pMsg = static_cast<const MSG_REQ_LOGIN*>(_pMsg);
		 char szId[MAX_ID_LEN + 1] = { '\0', };
		 char szPw[MAX_PW_LEN + 1] = { '\0', };

		 // 메시지로부터 ID와 PW데이터를 추출한다.
		 memcpy(szId, pMsg->Email, sizeof(char) * pMsg->id_len);		// id
		 memcpy(szPw, pMsg->Password, sizeof(char) * MAX_PW_LEN);	// pw 복사

		 Log::i(_T("Login..."));
		 Log::v(_T("ID(Email): %s"), szId);

		 s << "SELECT IDX FROM MEMBERSHIPS WHERE ID='" << szId << "' AND PW='" << szPw << "';";
		 SEND_QUERY_RESULT(s, result);
		 if (result.IsEmpty())
		 {
			 // 아이디가 존재하지 않거나 패스워드가 틀리다.
			 Log::i(_T("Login Failed"));
			 Log::v(_T("ID(Email): %s"), szId);
			 RESP_RESULT_REFUSAL(REQ_LOGIN);
		 }
		 else
		 {
			 // 로그인을 했다는건 유저토큰이 존재하지 않는다는 것
			 // 유저토큰을 보내준다. -> 이제부더 자동로그인됨
			 TOKEN usertoken = _atoi64(result[0][0].c_str());
			 // 세션에 유저토큰 정보를 설정
			 _pSession->GetClient().SetUser(usertoken);
			 s << "UPDATE MEMBERSHIPS SET LASTLOGINDATE='" << Server::NowDateTimeString().c_str() << "', DEVICETOKEN='" << _pSession->GetClient().GetDevice() << "' WHERE IDX='" << usertoken << "';";

			 // 디바이스토큰 및 접속시각 업데이트
			 SEND_QUERY(s);

			 MSG_RESP_USERTOKEN user(usertoken);
			 _pSession->PostSend((char*) &user, user.nSize);

			 Log::i(_T("Login Successed"));
			 Log::v(_T("id(Email): %s"), szId);
		 }

		 break;
	 }	// REG_LOGIN
#pragma endregion
#pragma region REQ_LOGOUT
	 case REQ_LOGOUT:
	 {
		 const MSG_REQ_LOGOUT* pMsg = static_cast<const MSG_REQ_LOGOUT*>(_pMsg);
		 // 로그아웃 -> 사용자 계정 정보에서 디바이스 토큰 정보를 삭제하는 것 -> 0으로 만듬

		 TOKEN nDevice = _pSession->GetClient().GetDevice();

		 // 세션정보의 유저토큰을 초기화
		 _pSession->GetClient().Clear();
		 // 디바이스정보는 초기화되면 안됨
		 _pSession->GetClient().SetDevice(nDevice);

		 s << "UPDATE MEMBERSHIPS SET DEVICETOKEN='" << 0 << "' WHERE IDX = '" << _pSession->GetClient().GetUser() << "';";
		 SEND_QUERY(s);


		 RESP_RESULT_APPROVED(REQ_LOGOUT);
		 break;	// REQ_LOGOUT
	 }
#pragma endregion
#pragma region REQ_LOCATIONDATA
	 case REQ_LOCATIONDATA:
	 {
		 const MSG_REQ_LOCATIONDATA* pMsg = static_cast<const MSG_REQ_LOCATIONDATA*>(_pMsg);

		 s << "SELECT IDX, LATITUDE, LONGITUDE, SPEED, BEARING, ACCURACY, DATETIME FROM LOCATIONDATAS WHERE ";
		 s << "DATETIME >= '" << pMsg->beginpoint.GetString().c_str() << "' AND '" << pMsg->endpoint.GetString().c_str() << "' AND ";
		 s << "IDX IN (SELECT END FROM LINKS WHERE START='" << _pSession->GetClient().GetUser() << "')";
		 s << " ORDER BY IDX ASC;";

		 SEND_QUERY_RESULT(s, result);
		 size_t nRowSize = result.GetRowSize();
		 if (nRowSize <= 0)
		 {
			 RESP_RESULT_APPROVED(REQ_LOCATIONDATA);
		 }

		 LOCATIONDATA* pLocList = new LOCATIONDATA[nRowSize];
		 for (int i = 0; i < nRowSize; i++)
			 pLocList[i] = LOCATIONDATA(result[i][0].c_str(), result[i][1].c_str(), result[i][2].c_str(), result[i][3].c_str(), result[i][4].c_str(), result[i][5].c_str(), result[i][6].c_str());

		 MSG_RESP_LOCATIONDATA resp_loc(nRowSize, pLocList);
		 const char* pBuf = resp_loc.GetBuffer();

		 _pSession->PostSend(pBuf, resp_loc.nSize);

		 SAFE_DEL_ARRAY(pLocList);
		 SAFE_DEL_ARRAY(pBuf);
		 break;
	 }	// case REQ_LOCATIONDATA
#pragma endregion
#pragma region REQ_GETALARMZONE
	 case REQ_GETALARMZONE:
	 {
		 const MSG_REQ_GETALARMZONE* pMsg = static_cast<const MSG_REQ_GETALARMZONE*>(_pMsg);

		 s << "SELECT ZONE_IDX, LATITUDE, LONGITUDE, DISTANCE, NICKNAME, DATETIME FROM ALARMZONES WHERE IDX='" << _pSession->GetClient().GetUser() << "';";
		 SEND_QUERY_RESULT(s, result);

		 size_t cnt = result.GetRowSize();
		 if (cnt <= 0)
		 {
			 RESP_RESULT_APPROVED(REQ_GETALARMZONE);
		 }

		 ALARMZONE* pZones = new ALARMZONE[cnt];
		 for (int i = 0; i < cnt; i++)
			 pZones[i] = ALARMZONE(result[i][0].c_str(), result[i][1].c_str(), result[i][2].c_str(), result[i][3].c_str(), result[i][4].c_str(), result[i][4].length());

		 MSG_RESP_ALARMZONE resp(cnt, pZones);
		 const char* pBuf = resp.GetBuffer();

		 _pSession->PostSend(pBuf, resp.nSize);
		 SAFE_DEL_ARRAY(pBuf);
		 SAFE_DEL_ARRAY(pZones);
		 break;
	 }	// REQ_GETALARMZONE
#pragma endregion
#pragma region REQ_ADDALARMZONE
	 case REQ_ADDALARMZONE:
	 {
		 const MSG_REQ_ADDALARMZONE* pMsg = static_cast<const MSG_REQ_ADDALARMZONE*>(_pMsg);

		 // 알람존 닉네임 가져오기
		 char buf[MAX_NICK_LEN + 1] = { '\0', };
		 memcpy(buf, pMsg->nick, sizeof(char) * MAX_NICK_LEN);

		 // 쿼리
		 s << setprecision(20);
		 s << "INSERT INTO ALARMZONES (IDX, LATITUDE, LONGITUDE, DISTANCE, NICKNAME, DATETIME) VALUES('";
		 s << _pSession->GetClient().GetUser() << "', '" << pMsg->latitude << "', '" << pMsg->longitude << "', '";
		 s << pMsg->distance << "', '" << buf << "', '" << Server::NowDateTimeString().c_str() << "');";
		 SEND_QUERY(s);

		 s << "SELECT ZONE_IDX, NICKNAME FROM ALARMZONES WHERE IDX='" << _pSession->GetClient().GetUser() << "' ORDER BY DATETIME DESC LIMIT 1;";
		 SEND_QUERY_RESULT(s, result);
		 if (result.IsEmpty())
		 {
			 RESP_RESULT_SERVERERR(REQ_ADDALARMZONE);
		 }

		 MSG_RESP_ADDALARMZONE msg(_atoi64(result[0][0].c_str()));

		 // 성공
		 _pSession->PostSend((char*) &msg, msg.nSize);
		 break;
	 }	// case REQ_ADDALARMZONE
#pragma endregion
#pragma region REQ_GETMYDATA
	 case REQ_GETMYDATA:
	 {
		 // DB로부터 ID와 NAME정보를 가져옴
		 s << "SELECT ID, NAME FROM MEMBERSHIPS WHERE IDX='" << _pSession->GetClient().GetUser() << "'AND DEVICETOKEN='" << _pSession->GetClient().GetDevice() << "';";
		 SEND_QUERY_RESULT(s, result);

		 if (result.IsEmpty())
		 {
			 RESP_RESULT_REFUSAL(REQ_GETMYDATA);	// DB에서 데이터를 찾을 수 없음
		 }
		 else
		 {
			 // 쿼리결과중 0번째 행은 ID, 1번째 행은 이름이로다.
			 MSG_RESP_GETMYDATA msg(_pSession->GetClient().GetDevice(), result[0][0].size(), result[0][0].c_str(), result[0][1].size(), result[0][1].c_str());
			 const char* buf = msg.GetBuffer();

			 _pSession->PostSend(buf, msg.nSize);
			 SAFE_DEL_ARRAY(buf);
		 }

		 break;
	 }	// case REQ_GETMYDATA
#pragma endregion
#pragma region REQ_GETFRIENDREQLIST
	 case REQ_GETFRIENDREQLIST:
	 {
		 TOKEN usertoken = _pSession->GetClient().GetUser();
		 // s << "SELECT ID, NAME, DATETIME FROM MEMBERSHIPS NATURAL JOIN REQUESTFRIENDS WHERE END='" << usertoken << "' AND IDX!='" << usertoken << "';";
		 s << "SELECT ID, NAME FROM MEMBERSHIPS WHERE IDX IN (SELECT START FROM REQUESTFRIENDS WHERE END='" << usertoken << "');";
		 SEND_QUERY_RESULT(s, result);
		 if (result.IsEmpty())
		 {
			 RESP_RESULT(REQ_GETFRIENDREQLIST, NOTFOUND);	// 찾지 못했음
		 }
		 // 왠진 모르겠지만 쿼리 한방으로 안됨. 개빢침... 나중에 고쳐야지
		 QueryResult result_datetime;
		 s << "SELECT DATETIME FROM REQUESTFRIENDS WHERE END='" << usertoken << "' AND START IN (SELECT START FROM REQUESTFRIENDS WHERE END='" << usertoken << "');";
		 SEND_QUERY_RESULT(s, result_datetime);
		 if (result_datetime.IsEmpty())
		 {
			 RESP_RESULT(REQ_GETFRIENDREQLIST, NOTFOUND);	// 찾지 못했음
		 }

		 size_t nRows = result.GetRowSize();
		 FRIENDREQLIST* pList = new FRIENDREQLIST[nRows];
		 for (int i = 0; i < nRows; i++)
		 {
			 std::string strEmail = result[i][0];
			 std::string strName = result[i][1];
			 DATETIME_STRING datetime;
			 memcpy(datetime.datetime, &result_datetime[i][0][0], MAX_DATETIME_LEN);

			 pList[i] = FRIENDREQLIST(strEmail.size(), strName.size(), datetime, strEmail.c_str(), strName.c_str());
		 }

		 MSG_RESP_GETFRIENDREQLIST msg(pList, nRows);
		 const char* pBuf = msg.GetBuffer();
		 _pSession->PostSend(pBuf, msg.nSize);

		 SAFE_DEL_ARRAY(pList);
		 SAFE_DEL_ARRAY(pBuf);
		 break;
	 }	// case REQ_GETFRIENDREQLIST
#pragma endregion
#pragma region REQ_GETFRIENDLIST
	 case REQ_GETFRIENDLIST:
	 {
		 TOKEN usertoken = _pSession->GetClient().GetUser();
		 s << "SELECT ID, NAME FROM MEMBERSHIPS WHERE IDX IN (SELECT END FROM LINKS WHERE START='" << usertoken << "');";

		 SEND_QUERY_RESULT(s, result);
		 if (result.IsEmpty())
		 {
			 RESP_RESULT(REQ_GETFRIENDREQLIST, NOTFOUND);	// 찾지 못했음
		 }

		 QueryResult result_datetime;
		 s << "SELECT LINKS_DATETIME, RELATIONSHIP FROM LINKS WHERE START='" << usertoken << "';";
		 SEND_QUERY_RESULT(s, result_datetime);
		 if (result_datetime.IsEmpty())
		 {
			 RESP_RESULT(REQ_GETFRIENDREQLIST, NOTFOUND);	// 찾지 못했음
		 }

		 size_t nRows = result.GetRowSize();
		 FRIENDLIST* pList = new FRIENDLIST[nRows];
		 for (int i = 0; i < nRows; i++)
		 {
			 std::string strEmail = result[i][0];
			 std::string strName = result[i][1];
			 DATETIME_STRING datetime;
			 memcpy(datetime.datetime, &result_datetime[i][0][0], MAX_DATETIME_LEN);
			 std::string strRelationship = result_datetime[i][1];

			 pList[i] = FRIENDLIST(strEmail.size(), strName.size(), strRelationship.size(), datetime, strEmail.c_str(), strName.c_str(), strRelationship.c_str());
		 }

		 MSG_RESP_GETFRIENDLIST msg(pList, nRows);
		 const char* pBuf = msg.GetBuffer();
		 _pSession->PostSend(pBuf, msg.nSize);

		 SAFE_DEL_ARRAY(pList);
		 SAFE_DEL_ARRAY(pBuf);

		 break;
	 }	// case REQ_GETFRIENDLIST
#pragma endregion
#pragma region REQ_ADDFRIEND
	 case REQ_ADDFRIEND:
	 {
		 const MSG_REQ_ADDFRIEND* pMsg = static_cast<const MSG_REQ_ADDFRIEND*>(_pMsg);
		 char szMail[MAX_ID_LEN] = { '\0', };
		 memcpy(szMail, pMsg->email, pMsg->email_len);

		 // mail의 idx를 받아옴
		 s << "SELECT IDX FROM MEMBERSHIPS WHERE ID='" << szMail << "';";
		 SEND_QUERY_RESULT(s, result);

		 if (result.IsEmpty())
		 {
			 RESP_RESULT(REQ_ADDFRIEND, NOTFOUND);
		 }
		 TOKEN targ_token = _atoi64(result[0][0].c_str());
		 result.Clear();

		 // 이미 친구등록 되어있는지 확인
		 s << "SELECT LINKS_IDX FROM LINKS WHERE START='" << _pSession->GetClient().GetUser() << "' AND END='" << targ_token << "';";
		 SEND_QUERY_RESULT(s, result);

		 // 이미 등록되어 있음
		 if (!result.IsEmpty())
		 {
			 RESP_RESULT(REQ_ADDFRIEND, ALREADY);
		 }

		 // 친구요청 테이블에 등록
		 s << "INSERT INTO REQUESTFRIENDS(START, END, DATETIME) VALUES('";
		 s << _pSession->GetClient().GetUser() << "', '" << targ_token << "', '" << NowDateTimeString().c_str() << "');";

		 SEND_QUERY(s);

		 result.Clear();

		 // 푸시를 위해서
		 s << "SELECT REGID FROM DEVICES WHERE DEVICE_IDX = (SELECT DEVICETOKEN FROM MEMBERSHIPS WHERE IDX='" << targ_token << "');";
		 SEND_QUERY_RESULT(s, result);
		 if (result.IsEmpty())
			 break;

		 GCMServer::Push(result[0][0].c_str(), "msg", "친구요청이 도착했어요!");

		 RESP_RESULT_APPROVED(REQ_ADDFRIEND);

		 break;
	 }	// case REQ_ADDFRIEND
#pragma endregion
#pragma region REQ_RESPFRIENDREQ
	 case REQ_RESPFRIENDREQ:
	 {
		 const MSG_REQ_RESPFRIENDREQ* pMsg = static_cast<const MSG_REQ_RESPFRIENDREQ*>(_pMsg);

		 // 메일주소를 카피
		 int nEmailLen = pMsg->nEmailLen;
		 char szMail[MAX_ID_LEN] = { '\0', };
		 memcpy(szMail, pMsg->szMail, nEmailLen);

		 // 쿼리
		 s << "SELECT REQF_IDX, START, END FROM REQUESTFRIENDS WHERE END='" << _pSession->GetClient().GetUser() <<
			 "' AND START=(SELECT IDX FROM MEMBERSHIPS WHERE ID='" << szMail << "');";
		 SEND_QUERY_RESULT(s, result);
		 if (result.IsEmpty())
		 {
			 // 왜 없을까요? 일단 거절
			 RESP_RESULT_REFUSAL(REQ_RESPFRIENDREQ);
		 }

		 switch (pMsg->resp)
		 {
		 case REQUEST_RESULT::REFUSAL:	// 거부함
		 {
			 // 친구요청목록에서 삭제함
			 s << "DELETE FROM REQUESTFRIENDS WHERE REQF_IDX='" << atoi(result[0][0].c_str()) << "';";
			 SEND_QUERY(s);
			 RESP_RESULT_APPROVED(REQ_RESPFRIENDREQ);
			 break;
		 }	// case REQUEST_RESULT::REFUSAL
		 case REQUEST_RESULT::APPROVED:	// 승낙함
		 {
			 // 결과값은 하나만 나와야함.
			 // 서로를 친구로 추가함
			 std::string strNow = NowDateTimeString();
			 TOKEN start = _atoi64(result[0][1].c_str());
			 TOKEN end = _atoi64(result[0][2].c_str());
			 s << "INSERT INTO LINKS(START, END, LINKS_DATETIME) VALUES('" << start << "', '" << end << "', '" << strNow.c_str() << "'), ";
			 s << "('" << end << "', '" << start << "', '" << strNow.c_str() << "');";
			 // 쿼리
			 SEND_QUERY(s);
			 // 응답
			 RESP_RESULT_APPROVED(REQ_RESPFRIENDREQ);
			 break;
		 }	// case REQUEST_RESULT::APPROVED
		 }

		 break;
	 }	// case REQ_RESPFRIENDREQ
#pragma endregion
#pragma region CTS_GPSDATA
	 case CTS_GPSDATA:
	 {
		 const MSG_CTS_GPSDATA* pMsg = static_cast<const MSG_CTS_GPSDATA*>(_pMsg);

		 s << setprecision(20);
		 s << "INSERT INTO LOCATIONDATAS(IDX, LATITUDE, LONGITUDE, SPEED, BEARING, ACCURACY, DATETIME) VALUES('";
		 s << _pSession->GetClient().GetUser() << "','" << pMsg->latitude << "','" << pMsg->longitude << "','" << pMsg->speed << "','" <<
			 pMsg->bearing << "','" << pMsg->accuracy << "','" << pMsg->datetime.GetString() << "');";

		 SEND_QUERY(s);

		 // 푸시메시지 처리

		 RESP_RESULT_APPROVED(CTS_GPSDATA);
		 break;
	 }	// case CTS_GPSDATA
#pragma endregion
#pragma region CTS_REGID
	 case CTS_REGID:
	 {
		 const MSG_CTS_REGID* pMsg = static_cast<const MSG_CTS_REGID*>(_pMsg);
		 char* pRegid = new char[pMsg->regid_len + 1];
		 memset(pRegid, 0, sizeof(char) * (pMsg->regid_len + 1));
		 memcpy(pRegid, pMsg->buf, sizeof(char) * pMsg->regid_len);

		 _pSession->GetClient().SetRegId(pRegid, pMsg->regid_len);

		 s << "UPDATE DEVICES SET REGID='" << pRegid << "' WHERE DEVICE_IDX = '" << _pSession->GetClient().GetDevice() << "';";
		 SEND_QUERY(s);

		 RESP_RESULT_APPROVED(CTS_REGID);

		 break;
	 }	// case CTS_REGID
#pragma endregion
	 }
 }

tm Server::GetDateTimeFromString(const DATETIME_STRING& _rDt)
{
	char buf[MAX_DATETIME_LEN + 1] = { '\0', };
	memcpy(buf, _rDt.datetime, sizeof(char) * MAX_DATETIME_LEN);

	return GetDateTimeFromString(buf);
}

tm Server::GetDateTimeFromString(const char* _pszDatetime)
{
	boost::posix_time::ptime t = boost::posix_time::time_from_string(std::string(_pszDatetime));
	return boost::posix_time::to_tm(t);
}

tm Server::NowDateTime()
{
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::time_t t = std::chrono::system_clock::to_time_t(now);
	boost::posix_time::ptime posix_t = boost::posix_time::from_time_t(t);
	return boost::posix_time::to_tm(posix_t);
}

std::string Server::NowDateTimeString()
{
	std::stringstream strDatetime;
	tm now = Server::NowDateTime();
	strDatetime << now.tm_year + 1900 << "-" << now.tm_mon + 1 << "-" << now.tm_mday << " " << now.tm_hour << ":" << now.tm_min << ":" << now.tm_sec;
	return strDatetime.str();
}

