-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        5.5.5-10.0.10-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win32
-- HeidiSQL 버전:                  8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- project 의 데이터베이스 구조 덤핑
CREATE DATABASE IF NOT EXISTS `project` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `project`;


-- 테이블 project의 구조를 덤프합니다. link_list
CREATE TABLE IF NOT EXISTS `link_list` (
  `identifier` int(10) unsigned NOT NULL,
  `uuid` char(36) NOT NULL,
  PRIMARY KEY (`identifier`,`uuid`),
  KEY `uuid_FK` (`uuid`),
  CONSTRAINT `identifier_FK` FOREIGN KEY (`identifier`) REFERENCES `user_list` (`identifier`),
  CONSTRAINT `uuid_FK` FOREIGN KEY (`uuid`) REFERENCES `sender_list` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='client and sender''s link table';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 project의 구조를 덤프합니다. locationdata_list
CREATE TABLE IF NOT EXISTS `locationdata_list` (
  `uuid` char(36) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `speed` int(11) NOT NULL,
  `bearing` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `accuracy` int(11) NOT NULL,
  KEY `FK_location_uuid` (`uuid`),
  CONSTRAINT `FK_location_uuid` FOREIGN KEY (`uuid`) REFERENCES `sender_list` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Location Data Table';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 project의 구조를 덤프합니다. safezone_table
CREATE TABLE IF NOT EXISTS `safezone_table` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` int(10) unsigned NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `distance` double NOT NULL,
  `nickname` varchar(50) NOT NULL,
  PRIMARY KEY (`idx`),
  KEY `safezone_identifier_FK` (`identifier`),
  CONSTRAINT `safezone_identifier_FK` FOREIGN KEY (`identifier`) REFERENCES `user_list` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='safezone set table';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 project의 구조를 덤프합니다. sender_list
CREATE TABLE IF NOT EXISTS `sender_list` (
  `uuid` char(36) NOT NULL COMMENT 'sender identification id',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='location data sender list';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 project의 구조를 덤프합니다. speedlimit_table
CREATE TABLE IF NOT EXISTS `speedlimit_table` (
  `identifier` int(10) unsigned NOT NULL COMMENT 'speedlimit set client',
  `uuid` char(36) NOT NULL COMMENT 'target sender',
  `speed` smallint(6) NOT NULL COMMENT 'limit value',
  PRIMARY KEY (`identifier`,`uuid`),
  KEY `speedlimit_uuid_FK` (`uuid`),
  CONSTRAINT `speedlimit_identifier_FK` FOREIGN KEY (`identifier`) REFERENCES `user_list` (`identifier`),
  CONSTRAINT `speedlimit_uuid_FK` FOREIGN KEY (`uuid`) REFERENCES `sender_list` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='speed limit data table';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 project의 구조를 덤프합니다. user_list
CREATE TABLE IF NOT EXISTS `user_list` (
  `identifier` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'user''s identifier code',
  `id` char(12) NOT NULL COMMENT 'login id',
  `pw` char(40) NOT NULL COMMENT 'password',
  `regid` text COMMENT 'lastaccess phone appid',
  `accesstime` datetime DEFAULT NULL COMMENT 'lastaccess datetime',
  PRIMARY KEY (`identifier`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
