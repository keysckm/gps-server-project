-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        5.5.5-10.0.12-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win64
-- HeidiSQL 버전:                  8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- myapp 의 데이터베이스 구조 덤핑
CREATE DATABASE IF NOT EXISTS `myapp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `myapp`;


-- 테이블 myapp의 구조를 덤프합니다. alarmdatas
CREATE TABLE IF NOT EXISTS `alarmdatas` (
  `ZONEDATA_IDX` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '알람존 정보의 식별번호',
  `ZONE_IDX` int(10) unsigned NOT NULL COMMENT '알람존의 식별자',
  `IDX` bigint(20) unsigned NOT NULL COMMENT '알람대상이 되는 멤버의 토큰',
  PRIMARY KEY (`ZONEDATA_IDX`),
  KEY `FK_ALARMDATAS_IDX` (`IDX`),
  KEY `FK_ALARMDATAS_ZONEIDX` (`ZONE_IDX`),
  CONSTRAINT `FK_ALARMDATAS_ZONE_IDX` FOREIGN KEY (`ZONE_IDX`) REFERENCES `alarmzones` (`ZONE_IDX`) ON DELETE CASCADE,
  CONSTRAINT `FK_ALARMDATAS_IDX` FOREIGN KEY (`IDX`) REFERENCES `memberships` (`IDX`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='각 알람존의 대상자';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 myapp의 구조를 덤프합니다. alarmzones
CREATE TABLE IF NOT EXISTS `alarmzones` (
  `ZONE_IDX` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '알람존의 인덱스',
  `IDX` bigint(20) unsigned NOT NULL COMMENT '멤버 식별 토큰',
  `LATITUDE` double NOT NULL COMMENT '알람구역의 위도',
  `LONGITUDE` double NOT NULL COMMENT '알람구역의 경도',
  `DISTANCE` int(10) unsigned NOT NULL COMMENT '알람구역의 범위(반경/m단위)',
  `NICKNAME` varchar(50) NOT NULL COMMENT '알람구역의 별명',
  `DATETIME` datetime NOT NULL COMMENT '설정된 날짜',
  PRIMARY KEY (`ZONE_IDX`),
  KEY `FK_ALARMZONES_IDX` (`IDX`),
  CONSTRAINT `FK_ALARMZONES_IDX` FOREIGN KEY (`IDX`) REFERENCES `memberships` (`IDX`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='알람구역 리스트';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 myapp의 구조를 덤프합니다. devices
CREATE TABLE IF NOT EXISTS `devices` (
  `DEVICE_IDX` bigint(20) unsigned NOT NULL COMMENT '각 디바이스의 고유식별값',
  `REGID` text COMMENT 'GCM 서비스를 위한 App Reg ID',
  `LASTACCESS` datetime DEFAULT NULL COMMENT '디바이스가 서버에 마지막으로 엑세스한 날',
  PRIMARY KEY (`DEVICE_IDX`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 myapp의 구조를 덤프합니다. links
CREATE TABLE IF NOT EXISTS `links` (
  `LINKS_IDX` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'LINK 데이터의 인덱스',
  `START` bigint(20) unsigned NOT NULL COMMENT 'START가 END를 친구를 맺는다.',
  `END` bigint(20) unsigned NOT NULL,
  `RELATIONSHIP` varchar(50) DEFAULT NULL COMMENT 'START에게 있어 END의 관계',
  `LINKS_DATETIME` datetime NOT NULL COMMENT '관계를 맺은 날(?)',
  PRIMARY KEY (`LINKS_IDX`),
  KEY `FK_LINKS_START` (`START`),
  KEY `FK_LINKS_END` (`END`),
  CONSTRAINT `FK_LINKS_END` FOREIGN KEY (`END`) REFERENCES `memberships` (`IDX`) ON DELETE CASCADE,
  CONSTRAINT `FK_LINKS_START` FOREIGN KEY (`START`) REFERENCES `memberships` (`IDX`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='각 맴버들 간의 관계';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 myapp의 구조를 덤프합니다. locationdatas
CREATE TABLE IF NOT EXISTS `locationdatas` (
  `LOC_IDX` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'LOCATIONDATA 식별번호',
  `IDX` bigint(20) unsigned NOT NULL COMMENT '멤버 식별 토큰',
  `LATITUDE` double NOT NULL COMMENT '위도',
  `LONGITUDE` double NOT NULL COMMENT '경도',
  `SPEED` int(10) unsigned NOT NULL COMMENT '속도(Km/h)',
  `BEARING` smallint(5) unsigned NOT NULL COMMENT '정북방향을 0도 기준으로 시계방향 0~359',
  `ACCURACY` smallint(5) unsigned NOT NULL COMMENT '정확도(위경도를 중심으로 한 반경(m단위))',
  `DATETIME` datetime NOT NULL COMMENT '위치데이터가 들어온 시각',
  PRIMARY KEY (`LOC_IDX`),
  KEY `FK_LOCATIONDATAS_IDX` (`IDX`),
  CONSTRAINT `FK_LOCATIONDATAS_IDX` FOREIGN KEY (`IDX`) REFERENCES `memberships` (`IDX`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='위치정보 데이터 테이블';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 myapp의 구조를 덤프합니다. memberships
CREATE TABLE IF NOT EXISTS `memberships` (
  `IDX` bigint(20) unsigned NOT NULL COMMENT '멤버 식별 토큰',
  `ID` varchar(32) NOT NULL COMMENT '로그인 아이디(이메일)',
  `PW` varchar(40) NOT NULL COMMENT 'SHA-1로 암호화된 패스워드',
  `NAME` varchar(50) NOT NULL COMMENT '이름',
  `DEVICETOKEN` bigint(20) unsigned DEFAULT NULL COMMENT '디바이스 토큰',
  `CREATEDATE` date NOT NULL COMMENT '가입일 DATE',
  `LASTLOGINDATE` datetime DEFAULT NULL COMMENT '마지막 로그인 DATETIME',
  `PWCHANGEDATE` date NOT NULL COMMENT '패스워드를 변경한날 DATE',
  PRIMARY KEY (`IDX`),
  UNIQUE KEY `ID` (`ID`),
  KEY `FK_MEMBERSHIPS_DEVICETOKEN` (`DEVICETOKEN`),
  CONSTRAINT `FK_MEMBERSHIPS_DEVICETOKEN` FOREIGN KEY (`DEVICETOKEN`) REFERENCES `devices` (`DEVICE_IDX`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='멤버 데이터 테이블';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 myapp의 구조를 덤프합니다. requestfriends
CREATE TABLE IF NOT EXISTS `requestfriends` (
  `REQF_IDX` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '친구요청데이터 인덱스',
  `START` bigint(20) unsigned NOT NULL COMMENT '요청자',
  `END` bigint(20) unsigned NOT NULL COMMENT '대상자',
  `DATETIME` datetime NOT NULL COMMENT '요청일',
  PRIMARY KEY (`REQF_IDX`),
  KEY `REQUESTFRIENDS_START_FK` (`START`),
  KEY `REQUESTFRIENDS_END_FK` (`END`),
  CONSTRAINT `REQUESTFRIENDS_END_FK` FOREIGN KEY (`END`) REFERENCES `memberships` (`IDX`) ON DELETE CASCADE,
  CONSTRAINT `REQUESTFRIENDS_START_FK` FOREIGN KEY (`START`) REFERENCES `memberships` (`IDX`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='친구요청 정보 테이블';

-- 내보낼 데이터가 선택되어 있지 않습니다.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
