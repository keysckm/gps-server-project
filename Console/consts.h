#ifndef __CONSTS_H__
#define __CONSTS_H__

#include "Token.h"

const unsigned short	SERVER_PORT = 32400;		// 통신 포트
const size_t			MAX_RECV_BUF_LEN = 1024;	// 수신 버퍼 크기
const size_t			MAX_DATETIME_LEN = 19;			// 시간 문자열 길이
const size_t			MAX_ID_LEN = 32;			// ID(email) 길이
const size_t			MAX_PW_LEN = 40;			// 패스워드 길이(SHA-1 Hash)
const size_t			MAX_REGID_LEN = 512;		// GCM RegId의 최대 길이
const size_t			MAX_NICK_LEN = 50;			// 닉네임 길이(Byte, 한글 기준 25글자 이하)
const size_t			MAX_NAME_LEN = 50;			// 이름 길이(Byte, 한글 기준 6글자 이하)

const TOKEN				INVALID_TOKEN = 0;
#endif