#include "stdafx.h"
#include "DATABASE_CTRL.h"

void DBCtrl::__init()
{
	mysql_init(&m_conn);	// db connection init
}

bool DBCtrl::conn(const char* _sz_db_name /* = nullptr */, const char* _sz_db_user /* = nullptr */, const char* _sz_db_pass /* = nullptr */, const char* _sz_db_host /* = nullptr */, const int _n_db_port /* = 0 */)
{
	const char* szDB = _sz_db_name ? _sz_db_name : DB_NAME;
	const char* szUSER = _sz_db_user ? _sz_db_user : DB_USER;
	const char* szPASS = _sz_db_pass ? _sz_db_pass : DB_PASS;
	const char* szHOST = _sz_db_host ? _sz_db_host : DB_HOST;
	const int nPORT = _n_db_port != 0 ? _n_db_port : DB_PORT;

	// db connect
	m_pConnect = mysql_real_connect(&m_conn,
		szHOST, szUSER, szPASS, szDB, nPORT, nullptr, 0);
	if (!m_pConnect)
	{
		Log::e(_T("데이터베이스와 연결에 실패했습니다."));
		Log::v(_T("errno: %d, errmsg: %s"), 
			mysql_errno(&m_conn), mysql_error(&m_conn));

		return false;
	}

	Log::i(_T("데이터베이스와 연결되었습니다."));
	Log::v(_T("db: %s, host: %s, port: %d"), szDB, szHOST, nPORT);

	SAFE_CONDI(!set_charset(CHARSET_UTF8)) false;

	return true;
}

bool DBCtrl::disconn()
{
	SAFE_PTR(m_pConnect) false;

	mysql_close(m_pConnect);
	Log::i(_T("데이터베이스 연결이 끊어졌습니다."));

	m_pConnect = nullptr;
	return true;
}

bool DBCtrl::set_charset(const char* _szCharset)
{
	// 한글을 이용하기위한 charset 변경
	std::string charset = _szCharset;
	std::string query_base = "SET SESSION character_set_";
	std::string query_target[3] = { "connection=", "results=", "client=" };

	for (int i = 0; i < 3; i++)
	{
		if (!send_query((query_base + query_target[0] + charset).c_str()))
			return false;
	}

	return true;
}

bool DBCtrl::send_query(const char* _szQuery)
{
	// mysql_query는 성공하면 0, 실패하면 그 외의 값을 리턴한다.
	if (mysql_query(m_pConnect, _szQuery))
	{
		Log::e(_T("데이터베이스가 쿼리를 거부하였습니다."));
		Log::v(_T("실패한 쿼리: %s"), _szQuery);
		Log::v(_T("errno: %d, errmsg: %s"), 
			mysql_errno(m_pConnect), mysql_error(m_pConnect));

		return false;
	}

	m_pResult = mysql_store_result(m_pConnect);
	if (m_pResult)
	{
		m_nResCnt = mysql_num_rows(m_pResult);
		m_nFieldCnt = mysql_num_fields(m_pResult);

		//m_Row = mysql_fetch_row(m_pResult);	// result에서 결과의 row를 받아온다.
		//m_pFields = mysql_fetch_fields(m_pResult);
	}

	return true;
}

bool DBCtrl::isConnect()
{
	return m_pConnect ? true : false;
}

DBCtrl::DBCtrl()
	: m_pConnect(nullptr)
	, m_pResult(nullptr)
	, m_nQuery_state(0)
	, m_nResCnt(0)
	, m_nFieldCnt(0)
{
	__init();
}


DBCtrl::~DBCtrl()
{
	disconn();
}