#include "stdafx.h"
#include "Connection.h"


Connection::Connection(boost::asio::io_service& _rIo)
	: m_sock(_rIo, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), SERVER_PORT))
{
}


Connection::~Connection()
{
}

void Connection::__handle_recv(const boost::system::error_code& _rErr, const size_t _nBytes_transferred)
{
	// 수신완료후 수신된 데이터를 처리한다.
	// 이 함수는 io_service 스레드에서 콜백
	if (_rErr)
	{
		if (_rErr == boost::asio::error::eof)
			Log::e(_T("Disconnected Connection."));

		Log::e(_T("Receive Error"));
		Log::v(_T("%s"), _rErr.message());
	}

	// UDP는 비연결지향 + 데이터의 연속성이 없는 소켓
	// 중간에 데이터가 뭐 하나 작살나거나 하기는 해도
	// 덜오거나 하진 않는다.

	Log::i("Recv", _nBytes_transferred);
	Log::v("from %s, byte: %d", m_ep.address().to_string().c_str(), _nBytes_transferred);

	// 메시지로 받음
	MSG_HEADER* pHeader = reinterpret_cast<MSG_HEADER*>(&m_RecvBuf[0]);
	UDPServer::ProcessPacket(this, pHeader);

	PostReceive();
}

void Connection::__handle_send(const boost::system::error_code& _rErr, const size_t _nBytes_transferred)
{
	std::tuple<char*, size_t> t = m_SendQueue.front();
	SAFE_DEL_ARRAY(std::get<0>(t));
	m_SendQueue.pop_front();

	if (_rErr)
	{
		if (_rErr == boost::asio::error::eof)
			Log::e(_T("Disconnected Connection."));

		Log::e(_T("Send Error"));
		Log::v(_T("%s"), _rErr.message());
	}

	if (m_SendQueue.empty())
		m_IsSend = false;
	else
	{
		std::tuple <char*, size_t> t = m_SendQueue.front();
		PostSend(std::get<0>(t), std::get<1>(t), true);
	}
}

void Connection::PostReceive()
{
	m_RecvBuf.assign(0);
	m_sock.async_receive_from(boost::asio::buffer(m_RecvBuf),
		m_ep, boost::bind(&Connection::__handle_recv, this,
		boost::asio::placeholders::error,
		boost::asio::placeholders::bytes_transferred));
}

void Connection::PostSend(const char* _pSendBuf, size_t _nSendSize, bool _IsContinue /* = false*/)
{
	char* pBuf;
	if (_IsContinue)
	{	// __handle_send 멤버 함수에 의해 호출됨
		// buf는 이미 동적할당 되어있음
		pBuf = const_cast<char*>(_pSendBuf);
	}
	else
	{
		pBuf = new char[_nSendSize];
		memcpy(pBuf, _pSendBuf, _nSendSize);
		m_lock.lock();
		m_SendQueue.push_back(std::tuple<char*, size_t>(pBuf, _nSendSize));
		m_lock.unlock();

		// 전송중, 콜백에서 PostSend를 다시 호출한다.
		if (m_IsSend)
			return;
	}

	m_IsSend = true;
	m_sock.async_send_to(boost::asio::buffer(_pSendBuf, _nSendSize),
		m_ep,
		boost::bind(&Connection::__handle_send, this,
		boost::asio::placeholders::error,
		boost::asio::placeholders::bytes_transferred)
		);
	Log::i("Send: %dbyte", _nSendSize);
}