#pragma once
class Convert
{
private:
	Convert();
	~Convert();
public:

	static std::wstring MBCStoUTF16(const char* _pszMultiByte)
	{
		// UTF16 = CP_ACP = widechar = wchar_t
		int nLen = MultiByteToWideChar(CP_ACP, 0, _pszMultiByte, strlen(_pszMultiByte), NULL, NULL);
		std::wstring strUnicode(nLen, '\0');
		MultiByteToWideChar(CP_ACP, 0, _pszMultiByte, strlen(_pszMultiByte), &strUnicode[0], nLen);

		return strUnicode;
	}

	static std::string UTF16toMBCS(const wchar_t* _pszwUtf16)
	{
		std::wstring strUtf16(_pszwUtf16);
		// MultiByte = CP_ACP = char
		// MultiByte가 되었을때 예상되는 크기 계산
		size_t buf_size = ::WideCharToMultiByte(CP_ACP, 0, strUtf16.c_str(), strUtf16.length(), NULL, 0, NULL, NULL);
		std::string strMultiByte(buf_size + 1, '\0');
		// 변환
		::WideCharToMultiByte(CP_ACP, 0, strUtf16.c_str(), strUtf16.length(), &strMultiByte[0], buf_size, NULL, NULL);

		return strMultiByte;
	}

	static std::string UTF8toMBCS(const char* _pszUtf8, size_t _nUtf8BufLen)
	{
		// UTF-8을 multibyte로 변환한다
		// UTF-8에서 multibyte로 바로 변환은 할 수 없다. utf-16(widechar)을 한번 건너야한다.
		// 변환 후의 예상데이터 크기 획득
		size_t nLen = ::MultiByteToWideChar(CP_UTF8, 0, _pszUtf8, _nUtf8BufLen, NULL, NULL);
		std::wstring strUTF16(nLen, '\0');
		// UTF8 -> UTF16
		::MultiByteToWideChar(CP_UTF8, 0, _pszUtf8, _nUtf8BufLen, &strUTF16[0], nLen);
		// 이제 UTF16을 MultiByte로 변환해야함
		// MultiByte가 되었을때 예상되는 크기 계산
		// MultiByte = CP_ACP = char
		nLen = ::WideCharToMultiByte(CP_ACP, 0, strUTF16.c_str(), strUTF16.length(), NULL, 0, NULL, NULL);
		std::string strMultiByte(nLen + 1, '\0');
		// 변환
		::WideCharToMultiByte(CP_ACP, 0, strUTF16.c_str(), strUTF16.length(), &strMultiByte[0], nLen, NULL, NULL);

		return strMultiByte;
	}

	static std::string MBCStoUTF8(const char* _pszMBCS, size_t _nMBCSLen)
	{
		// WideChar를 거쳐 UTF8로 변환한다.
		// 길이획득
		size_t nLen = ::MultiByteToWideChar(CP_ACP, 0, _pszMBCS, _nMBCSLen, NULL, NULL);
		std::wstring strUTF16(nLen, '\0');
		// MBCS -> UTF16
		::MultiByteToWideChar(CP_ACP, 0, _pszMBCS, _nMBCSLen, &strUTF16[0], nLen);

		// UTF16을 UTF8로 변환
		// 길이획득
		nLen = ::WideCharToMultiByte(CP_UTF8, 0, strUTF16.c_str(), strUTF16.length(), NULL, NULL, NULL, NULL);
		std::string strUTF8(nLen, '\0');
		// UTF16 -> UTF8
		::WideCharToMultiByte(CP_UTF8, 0, strUTF16.c_str(), strUTF16.length(), &strUTF8[0], nLen, NULL, NULL);

		return strUTF8;
	}
};

