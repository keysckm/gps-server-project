#pragma once

namespace GPS
{
	// invariable data
	const float		CONVERT_KNOT_KMh = 1.852f;	// KNOT <-> KM/h 변환 상수
	const size_t	MAX_SATELLITE_COUNT = 16;	// 최대 위성 갯수
	const size_t	MAX_SENTENCE_LENGTH = 6;
	const size_t	MAX_CMD_LEN = 8;			// Command 버퍼 길이
	const size_t	MAX_BUF_LEN = 128;			// 데이터 버퍼 길이
	const size_t	MAX_FIELD = 26;				// 필드 갯수

	enum SENTENCE_ID
	{
		SI_NONE = 0,
		SI_GPGGA,
		SI_GPGSV,
		SI_GPRMC,
	};

	enum POSITION_FIX
	{
		PF_INVALID	= 0,	// 무효한 좌표
		PF_VALID	= 1,	// 유효한 좌표
		PF_DGPS		= 2,	// 정밀도가 높은 좌표
		PF_PPS		= 3,	// ?
	};

	enum SATELLITE_STATE
	{
		SS_VALID	= 'A',	// 유효한 데이터
		SS_INVALID	= 'V',	// 무효한 데이터
	};

	struct date
	{
		unsigned short yy;	// 년
		unsigned short mm;	// 월
		unsigned short dd;	// 일
	};
	struct time
	{
		unsigned short hh;	// 시각
		unsigned short mm;	// 분
		unsigned short ss;	// 초
		unsigned short sss;	// 콤마초
	};

	struct GGA	// 시간, 경도, 위도, 시스템 품질, 위성수, 고도
	{
	//	데이터형		변수명				주석								포멧				기타
		time			utc;				// 세계표준시						hhmmss.sss
		float			latitude;			// 위도								ddmm.mmmm
		char			ns_indicator;		// N: 북위/S: 남위					N or S
		float			longitude;			// 경도								dddmm.mmmm
		char			ew_indicator;		// E: 동경/W: 서경					E or W
		POSITION_FIX	fix;				// 유효성							range is 1 ~ 3
		short			satellites;			// 위성수
		float			hdop;				// 수평오차												hdop * 10 = 오차거리(m)
		float			altitude;			// 고도								
		char			altitude_unit;		// ?								M
		float			geoid_seperation;	// ?
		char			sepration_unit;		// ?								M
		time			dpgs_age;			// dpgs 정보 갱신 시각				hhmmss.sss
		char			dpgs_stationid[4];	// dpgs 스테이션 id					0000
	};

	struct RMC	// 시간, 위도, 경도, 시스템장애, 속도, 경로, 날짜
	{
	//	데이터형		변수명			주석						포멧			기타
		time			utc;			// 세계표준시				hhmmss.sss
		SATELLITE_STATE	state;			// 유효성					A or V
		float			latitude;		// 위도						ddmm.mmmm
		char			ns_indicator;	// N: 북위/ S: 남위			N or S
		float			longitude;		// 경도						dddmm.mmmm
		char			ew_indicator;	// E: 동경/ W: 서경			E or W
		float			speed;			// speed over ground, 속도					1 knots = 0.51444m/s, 1851.984m/h, 1.852km/h
		float			course;			// 진행방향									정북을 기준으로 359도 계산
		date			date;			// 날짜						ddmmyy
		float			not_control_a;	// 나침반계 방향각							사용하지 않는다.
		char			not_control_b;	// 나침반계 방향			E or W			사용하지 않는다.
	};

	class BASE
	{
	protected:
		SENTENCE_ID m_nId;
		char		m_szComment[MAX_CMD_LEN];

	public:
		BASE(SENTENCE_ID _nId, const char* _szComment)
			: m_nId(_nId)
		{
			_tcscpy_s(m_szComment, MAX_CMD_LEN, _szComment);
		}

		virtual ~BASE()
		{}
		virtual bool Parse(_IN_ char* _pszData, size_t _nLen) = 0;

		inline SENTENCE_ID GetSentence() const
		{
			return m_nId;
		}
	};

	class GPGGA : public BASE
	{
	protected:
		GGA		m_data;

	public:
		GPGGA()
			: BASE(SI_GPGGA, "GPGGA")
		{
		}

		virtual ~GPGGA()
		{
		}

		virtual bool Parse(_IN_ char* _pszData, size_t _nLen);
	};

	class GPRMC : public BASE
	{
	protected:
		RMC		m_data;

	public:
		GPRMC()
			: BASE(SI_GPRMC, "GPRMC")
		{

		}

		virtual bool Parse(_IN_ char* _pszData, size_t _nLen);
		const RMC& GetData() const
		{
			return m_data;
		}
	};

	class GPS_UTIL
	{
	protected:
		enum PROC_STATE{
			PS_SEARCH,	// 시작 문자열 &를 찾는다.
			PS_COMMAND, // NMEA 프로토콜 판별
			PS_DATA,	// DATA 파싱
			PS_CHECKSUM,// 체크섬 계산
			PS_PARSE,	// 파싱
		};

	protected:
		std::deque<GPS::BASE*> m_GpsList;	// GPS 데이터 리스트

	protected:
		void __nmea(_IN_ char* _pszCmd, _IN_ char* _pszData, size_t _nDataLen);	// NMEA 파싱 및 GPS::BASE 객체 생성과 등록

	public:
		GPS_UTIL()
		{

		}
		~GPS_UTIL();

		static bool		str_to_time(_IN_ char* _pszTime, size_t _nBufSize, _OUT_ time& _rt);
		static bool		str_to_date(_IN_ char* _pszDate, size_t _nBufSize, _OUT_ date& _rd);
		
		static void		Revision_time(time& _rT, date& _rD, int _nOffset);
		
		static void		GetFields(_IN_ char* _pszData, _OUT_ char** _pszFields, size_t _nFIeldCnt, size_t _nDataLen);
		static float	GetFloat(_IN_ char* _pszBuf, float _default = 0.);
		static int		GetInt(_IN_ char* _pszBuf, int _default = 0);
		static char		GetChar(_IN_ char* _pszBuf, char _default = '\0');
		static void		GetString(_IN_ char* _pszData, char* _pszBuf, int _nBufSize);
		static float	GetLatitude(_IN_ char* _pszBuf, float _default = 0.);
		static float	GetLongitude(_IN_ char* _pszBuf, float _default = 0.);

		const BASE* operator[] (size_t _nIdx) const
		{
			return m_GpsList[_nIdx];
		}

		bool Parse(_IN_ char* _pszGPS, size_t _nLen);

		size_t GetGpsDataCnt()
		{
			return m_GpsList.size();
		}

		void RemoveFront()
		{
			BASE* pBase = m_GpsList.front();
			SAFE_DEL(pBase);

			m_GpsList.pop_front();
		}
	};
}

extern GPS::GPS_UTIL g_GpsUtil;