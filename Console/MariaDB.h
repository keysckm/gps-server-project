#pragma once

class QueryRow	// 하나의 열을 가지고있는 데이터
{
private:
	std::vector<std::string>	m_Columns;

public:
	QueryRow()
	{

	}

	QueryRow(MYSQL_ROW _MyRow, size_t _nRowCount)
	{
		for (int i = 0; i < _nRowCount; i++)
		{
// 			size_t len = strlen(_MyRow[i]);
// 			char* buf = new char[len + 1];
// 			strcpy(buf, _MyRow[i]);
			
			if (_MyRow[i])
				m_Columns.push_back(std::string(_MyRow[i]));
			else
				m_Columns.push_back(std::string(""));
		}
	}

	~QueryRow()
	{
		clear();
	}

	std::string operator [] (int index)
	{
		if (index >= m_Columns.size())
			return nullptr;

		return m_Columns[index];
	}

	void clear()
	{
		m_Columns.clear();
	}
};

class QueryResult
{
private:
	int		m_nField_size;			// 필드 갯수
	int		m_nRow_size;			// 열 갯수
	//char**	m_ppszColumnName;
	std::vector<QueryRow*>	m_pRows;

	QueryResult operator = (QueryResult& _rRef){}	// 대입생성 금지
	QueryResult(QueryResult& _rRef){}				// 복사생성 금지
	QueryResult(QueryResult&& _rrRef){}			// 우측값대입 생성 금지
private:

public:
	QueryResult()
	{
	}
	~QueryResult()
	{
		Clear();
	}
	bool IsEmpty()
	{
		if (m_nRow_size == 0)
			return true;
		return false;
	}
	size_t	GetRowSize()
	{
		return m_nRow_size;
	}
	void Clear()
	{
		for (QueryRow* pRow : m_pRows)
		{
			SAFE_DEL(pRow);
		}
		m_pRows.clear();
	}
	QueryRow& operator [] (int index)
	{
		if (index >= m_pRows.size())
		{
			static QueryRow row;
			return row;	// 의미없는 Row를 리턴한다.
		}

		return *m_pRows[index];
	}
	
	// MYSQL_RES(쿼리 결과)로 부터 결과값을 파싱
	void Parse(MYSQL_RES* _pMyRes)
	{
		m_nRow_size = mysql_num_rows(_pMyRes);
		m_nField_size = mysql_num_fields(_pMyRes);

		for (int i = 0; i < m_nRow_size; i++)
		{
			MYSQL_ROW row = mysql_fetch_row(_pMyRes);
			if (row)
				m_pRows.push_back(new QueryRow(row, m_nField_size));
		}
	}
};

const static char*	CHARSET_UTF8 = "utf8";
const static char*	CHARSET_EUCKR = "euckr";
const static char*	DB_HOST = "127.0.0.1";
const static int	DB_PORT = 3306;
const static char*	DB_NAME = "myapp";
const static char*	DB_USER = "root";
const static char*	DB_PASS = "whrudals";
class MariaDB
{
private:
	static MYSQL*			m_pConnect;
	static MYSQL			m_conn;

	static std::mutex		m_Lock;
public:
	MariaDB();
	~MariaDB();

	static bool Connect(const char* _szDBName = nullptr 
		, const char* _szUser = nullptr 
		, const char* _szPassword = nullptr 
		, const char* _szHost = nullptr 
		, const int _nPort = 0 );

	static void Disconnect();
	static bool SetCharset(const char* _szCharset);
	static bool IsConnected();

	// 결과값이 없는 쿼리들
	static bool	SendQuery(const char* _pszQuery);
	static bool	SendQuery(std::string& _rStr);
	static bool	SendQuery(std::stringstream& _rSs);

	// 결과값이 있는 쿼리들
	static bool	SendQuery(_IN_ const char* _pszQuery, _OUT_ QueryResult& _rResult);
	static bool	SendQuery(_IN_ std::string& _rStr, _OUT_ QueryResult& _rResult);
	static bool	SendQuery(_IN_ std::stringstream& _rSs, _OUT_ QueryResult& _rResult);

};

