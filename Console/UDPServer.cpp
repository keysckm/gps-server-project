#include "stdafx.h"
#include "UDPServer.h"


UDPServer::UDPServer()
{
}


UDPServer::~UDPServer()
{
}

UDPServer& UDPServer::GetServer()
{
	static UDPServer me;
	return me;
}

void UDPServer::Run()
{
	Log::i(_T(">>>>>>>>>> Server Running... <<<<<<<<<<"));
	Log::v(_T("Port no: %d"), SERVER_PORT);

	// 데이터베이스에 연결
	if (!MariaDB::Connect())
		return;

	// 동시에 여러 연결을 처리하기 위해 다수의 커넥션을 생성
	// 커넥션의 수는 나중에 동적으로 처리 가능하도록 변경
	AddConnection(1);

	// IO처리를 위해 스레드 추가
	AddIoRun(1);

	for (boost::thread* pThread : m_ThreadList)
	{
		pThread->join();
		SAFE_DEL(pThread);
	}
}

void UDPServer::AddConnection(size_t _nSize)
{
	for (int i = 0; i < _nSize; i++)
	{
		Connection* pConn = new Connection(m_ioserv);
		pConn->PostReceive();

		m_ConnectionList.push_back(pConn);
	}
}

void UDPServer::AddIoRun(size_t _nSize)
{
	for (int i = 0; i < _nSize; i++)
	{
		boost::thread* pThread = new boost::thread(
			boost::bind(&boost::asio::io_service::run,
			&m_ioserv)
			);

		m_ThreadList.push_back(pThread);
	}
}

// 서버에러
#define RESP_RESULT_SERVERERR_UDP(REQUEST)		{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::SERVERERR); \
	_pConn->PostSend((char*) &resp, resp.nSize);}
// 실패
#define RESP_RESULT_REFUSAL_UDP(REQUEST)		{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::REFUSAL); \
	_pConn->PostSend((char*) &resp, resp.nSize);}
// 성공
#define RESP_RESULT_APPROVED_UDP(REQUEST)		{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::APPROVED); \
	_pConn->PostSend((char*) &resp, resp.nSize);}
// 무효한 토큰
#define RESP_RESULT_INVALIDTOKNEN_UDP(REQUEST)	{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::INVALIDTOKEN); \
	_pConn->PostSend((char*) &resp, resp.nSize);}
// 기타
#define RESP_RESULT_UDP(REQUEST, RESULT)		{MSG_RESP_RESULT resp(REQUEST, REQUEST_RESULT::RESULT); \
	_pConn->PostSend((char*) &resp, resp.nSize);}
// 토큰 검사
#define CHK_TOKEN(REQUEST){if (!IsValidToken(pMsg->nDevice, pMsg->nUser))\
		{\
			RESP_RESULT_INVALIDTOKNEN_UDP(REQUEST);\
			break;\
		}}

// stringstream 클리어
#define CLEAR_SS(stringstream) stringstream.str(std::string()); stringstream.clear()	

// 결과가 없는 쿼리날리기, CLEAR_SS도 알아서 해준다.
#define SEND_QUERY(stringstream) {if (!MariaDB::SendQuery(stringstream)){ \
									RESP_RESULT_SERVERERR_UDP(_pMsg->nIdx);\
									break;}\
									CLEAR_SS(stringstream);}

#define SEND_QUERY_RESULT(stringstream, queryresult){if (!MariaDB::SendQuery(stringstream, queryresult)){ \
									RESP_RESULT_SERVERERR_UDP(_pMsg->nIdx);\
									break;}\
									CLEAR_SS(stringstream);}


bool UDPServer::IsValidToken(TOKEN _Device, TOKEN _User)
{
	std::stringstream s;
	QueryResult result;

	s << "SELECT ID FROM MEMBERSHIPS WHERE IDX='" << _User << "' AND DEVICETOKEN='" << _Device << "';";
	if (!MariaDB::SendQuery(s, result))
		return false;
	if (result.IsEmpty())
	{	// 토큰이 유효하지 않음
		Log::w("Invalid Token");
		Log::v("Device: %d, User: %d", _Device, _User);
		return false;
	}

	return true;
}

void UDPServer::ProcessPacket(Connection* _pConn, const MSG_HEADER* _pMsg)
{
	SAFE_PTR(_pConn);
	SAFE_PTR(_pMsg);

	QueryResult result;		// 쿼리 결과
	std::stringstream s;	// 문자열 스트림

	switch (_pMsg->nIdx)
	{
#pragma region REQ_ACCESS
	case REQ_ACCESS:
	{
		const MSG_REQ_ACCESS* pMsg = static_cast<const MSG_REQ_ACCESS*>(_pMsg);
		if (pMsg->nDevice == INVALID_TOKEN)	// 디바이스 토큰이 무효값이다. -> 첫 접속
		{
			TOKEN device = TokenGenerator::Generate();		// 토큰값 제네레이트
			std::string date = Server::NowDateTimeString();	// 쿼리
			s << "INSERT INTO DEVICES(DEVICE_IDX, LASTACCESS) VALUES('" << device << "', '" <<
				date.c_str() << "');";

			SEND_QUERY(s);

			MSG_RESP_DEVICETOKEN msg(device);			// 토큰 송신
			_pConn->PostSend((char*) &msg, msg.nSize);
			break;
		}
		else if (IsValidToken(pMsg->nDevice, pMsg->nUser))	// 디바이스 토큰과 유저 토큰이 유효함
		{
			// 바로 접속 허가
			s << "UPDATE DEVICES SET LASTACCESS='" << Server::NowDateTimeString() << "';";
			SEND_QUERY(s);

			RESP_RESULT_APPROVED_UDP(REQ_ACCESS);
			break;
		}
		else
		{
			// 디바이스 토큰이 존재하는데 토큰검사에서 탈락했다면 그냥 무효한 토큰
			RESP_RESULT_UDP(REQ_ACCESS, INVALIDTOKEN);
			break;
		}
		break;
	}	// case REQ_ACCESS
#pragma endregion
#pragma region REQ_SIGNUP
	case REQ_SIGNUP:
	{
		const MSG_REQ_SIGNUP* pSignUp = static_cast<const MSG_REQ_SIGNUP*>(_pMsg);

		char szId[MAX_ID_LEN + 1] = { '\0', };
		char szPw[MAX_PW_LEN + 1] = { '\0', };

		// 메시지로부터 ID와 PW데이터를 추출한다.
		memcpy(szId, pSignUp->Email, sizeof(char) * pSignUp->id_len);	// id
		memcpy(szPw, pSignUp->Password, sizeof(char) * MAX_PW_LEN);		// pw 복사

		// id 중복 검사
		s << "SELECT IDX FROM MEMBERSHIPS WHERE ID='" << szId << "';";
		SEND_QUERY_RESULT(s, result);
		if (!result.IsEmpty())
		{
			// 중복 ID 있음
			RESP_RESULT_REFUSAL_UDP(REQ_SIGNUP);

			Log::i("SignUp Failed");
			Log::v("ID is Exist");
			break;
		}

		TOKEN user = TokenGenerator::Generate();
		std::string datetime_string = Server::NowDateTimeString();
		// ID 중복 없음
		s << "INSERT INTO MEMBERSHIPS (IDX, ID, PW, CREATEDATE, PWCHANGEDATE) VALUES('" << user << "', '" << szId << "', '" << szPw << "', '" <<
			datetime_string << "', '" << datetime_string << "';";
		SEND_QUERY(s);
		RESP_RESULT_APPROVED_UDP(REQ_SIGNUP);
		Log::i("SignUp Successed");
		Log::v("id: %s", szId);
		break;
	}	// case REQ_SIGNUP
#pragma endregion
#pragma region REQ_LOGIN
	case REQ_LOGIN:
	{
		const MSG_REQ_LOGIN* pMsg = static_cast<const MSG_REQ_LOGIN*>(_pMsg);
		char szId[MAX_ID_LEN + 1] = { '\0', };
		char szPw[MAX_PW_LEN + 1] = { '\0', };

		// 메시지로부터 ID와 PW데이터를 추출한다.
		memcpy(szId, pMsg->Email, sizeof(char) * pMsg->id_len);		// id
		memcpy(szPw, pMsg->Password, sizeof(char) * MAX_PW_LEN);	// pw 복사

		Log::i("Login...");
		Log::v("ID(Email): %s", szId);

		s << "SELECT IDX FROM MEMBERSHIPS WHERE ID='" << szId << "' and pw='" << szPw << "';";
		SEND_QUERY_RESULT(s, result);
		if (result.IsEmpty())
		{
			// 아이디가 존재하지 않거나 패스워드가 틀리다.
			RESP_RESULT_REFUSAL_UDP(REQ_LOGIN);
			Log::i("Login Failed");
			Log::v("ID(Email): %s", szId);
		}
		else
		{

			// 로그인을 했다는건 유저토큰이 존재하지 않는다는 것
			// 유저토큰을 보내준다. -> 이제부더 자동로그인됨
			TOKEN usertoken = _atoi64(result[0][0]);
			s << "UPDATE MEMBERSHIPS SET LASTLOGINDATE='" << Server::NowDateTimeString().c_str() << "', DEVICETOKEN='" << pMsg->nDevice << "' WHERE IDX='" << usertoken << "';";

			// 디바이스토큰 및 접속시각 업데이트
			SEND_QUERY(s);

			MSG_RESP_USERTOKEN user(usertoken);
			_pConn->PostSend((char*) &user, user.nSize);

			Log::i("Login Successed");
			Log::v("id(Email): %s", szId);
		}

		break;
	}	// REG_LOGIN
#pragma endregion
#pragma region REQ_LOGOUT
	case REQ_LOGOUT:
	{
		const MSG_REQ_LOGOUT* pMsg = static_cast<const MSG_REQ_LOGOUT*>(_pMsg);
		// 로그아웃 -> 사용자 계정 정보에서 디바이스 토큰 정보를 삭제하는 것 -> 0으로 만듬
		
		s << "UPDATE MEMBERSHIPS SET DEVICETOKEN='" << 0 << "' WHERE IDX = '" << pMsg->nUser << "';";
		SEND_QUERY(s);

		RESP_RESULT_APPROVED_UDP(REQ_LOGOUT);
		break;	// REQ_LOGOUT
	}
#pragma endregion
#pragma region REQ_LOCATIONDATA
	case REQ_LOCATIONDATA:
	{
		const MSG_REQ_LOCATIONDATA* pMsg = static_cast<const MSG_REQ_LOCATIONDATA*>(_pMsg);
		CHK_TOKEN(REQ_LOCATIONDATA);

		// where 조건
		std::stringstream strWhere;
		s << "SELECT IDX, LATITUDE, LONGITUDE, SPEED, BEARING, ACCURACY, DATETIME FROM LOCATIONDATAS WHERE ";
		s << "DATETIME >= '" << pMsg->beginpoint.GetString().c_str() << "' AND '" << pMsg->endpoint.GetString().c_str() << "' AND ";
		int cnt = pMsg->IdxCnt;
		if (pMsg->IdxCnt <= 0)
		{	// 지정된 대상이 없는 경우
			s << "IDX IN (SELECT END FROM LINKS WHERE START='" << pMsg->nUser << "')";
		}
		else
		{	// 지정된 대상이 있는 경우
			strWhere << "IDX IN(";
			for (int i = 0; i < cnt-1; i++)
				strWhere << "'" << pMsg->pIdxArry[i] << "', ";
			strWhere << "'" <<  pMsg->pIdxArry[cnt-1] << "')";
		}
		s << strWhere.str();
		s << " ORDER BY IDX ASC;";

		SEND_QUERY_RESULT(s, result);
		size_t nRowSize = result.GetRowSize();
		if (nRowSize <= 0)
		{
			RESP_RESULT_APPROVED_UDP(REQ_LOCATIONDATA);
		}

		LOCATIONDATA* pLocList = new LOCATIONDATA[nRowSize];
		for (int i = 0; i < nRowSize; i++)
			pLocList[i] = LOCATIONDATA(result[i][0], result[i][1], result[i][2], result[i][3], result[i][4], result[i][5], result[i][6]);
				
		MSG_RESP_LOCATIONDATA resp_loc(nRowSize, pLocList);
		const char* pBuf = resp_loc.GetBuffer();
		
		_pConn->PostSend(pBuf, resp_loc.nSize);

		SAFE_DEL_ARRAY(pLocList);
		SAFE_DEL_ARRAY(pBuf);
		break;	
	}	// case REQ_LOCATIONDATA
#pragma endregion
#pragma region REQ_GETALARMZONE
	case REQ_GETALARMZONE:
	{
		const MSG_REQ_GETALARMZONE* pMsg = static_cast<const MSG_REQ_GETALARMZONE*>(_pMsg);
		CHK_TOKEN(REQ_GETALARMZONE);

		s << "SELECT ZONE_IDX, LATITUDE, LONGITUDE, DISTANCE, NICKNAME, DATETIME FROM ALARMZONES WHERE IDX='" << pMsg->nUser << "';";
		SEND_QUERY_RESULT(s, result);

		size_t cnt = result.GetRowSize();
		if (cnt <= 0)
		{
			RESP_RESULT_APPROVED_UDP(REQ_GETALARMZONE);
		}

		ALARMZONE* pZones = new ALARMZONE[cnt];
		for (int i = 0; i < cnt; i++)
			pZones[i] = ALARMZONE(result[i][0], result[i][1], result[i][2], result[i][3], result[i][4], strlen(result[i][4]));
		
		MSG_RESP_ALARMZONE resp(cnt, pZones);
		const char* pBuf = resp.GetBuffer();

		_pConn->PostSend(pBuf, resp.nSize); 
		SAFE_DEL_ARRAY(pBuf);
		SAFE_DEL_ARRAY(pZones);
		break;
	}	// REQ_GETALARMZONE
#pragma endregion
#pragma region REQ_ADDALARMZONE
	case REQ_ADDALARMZONE:
	{
		const MSG_REQ_ADDALARMZONE* pMsg = static_cast<const MSG_REQ_ADDALARMZONE*>(_pMsg);
		CHK_TOKEN(REQ_ADDALARMZONE);

		// 알람존 닉네임 가져오기
		char buf[MAX_NICK_LEN + 1] = { '\0', };
		memcpy(buf, pMsg->nick, sizeof(char) * MAX_NICK_LEN);

		// 쿼리
		s << setprecision(20);
		s << "INSERT INTO ALARMZONES (IDX, LATITUDE, LONGITUDE, DISTANCE, NICKNAME, DATATIME) VALUES('";
		s << pMsg->nUser << "', '" << pMsg->latitude << "', '" << pMsg->longitude << "', '";
		s << pMsg->distance << "', '" << buf << "', " << Server::NowDateTimeString().c_str() << "');";
		SEND_QUERY(s);

		s << "SELECT ZONE_IDX FROM ALARMZONES WHERE IDX='" << pMsg->nUser << "' ORDER BY DATETIME DESC LIMIT 1;";
		SEND_QUERY_RESULT(s, result);
		if (result.IsEmpty())
		{
			RESP_RESULT_SERVERERR_UDP(REQ_ADDALARMZONE);
		}

		MSG_RESP_ADDALARMZONE msg(_atoi64(result[0][0]));

		// 성공
		_pConn->PostSend((char*) &msg, msg.nSize);
		break;
	}	// REQ_ADDALARMZONE
#pragma endregion
#pragma region CTS_GPSDATA
	case CTS_GPSDATA:
	{
		const MSG_CTS_GPSDATA* pMsg = static_cast<const MSG_CTS_GPSDATA*>(_pMsg);
		CHK_TOKEN(CTS_GPSDATA);

		s << setprecision(20);
		s << "INSERT INTO LOCATIONDATAS(IDX, LATITUDE, LONGITUDE, SPEED, BEARING, ACCURACY, DATETIME) VALUES('";
		s << pMsg->nUser << "','" << pMsg->latitude << "','" << pMsg->longitude << "','" << pMsg->speed << "','" <<
			pMsg->bearing << "','" << pMsg->accuracy << "','" << pMsg->datetime.GetString();

		SEND_QUERY(s);

		// 푸시메시지 처리

		RESP_RESULT_APPROVED_UDP(CTS_GPSDATA);
		break;
	}	// case CTS_GPSDATA
#pragma endregion
#pragma region CTS_REGID
	case CTS_REGID:
	{
		const MSG_CTS_REGID* pMsg = static_cast<const MSG_CTS_REGID*>(_pMsg);
		char* pRegid = new char[pMsg->regid_len + 1];
		memset(pRegid, 0, sizeof(char) * (pMsg->regid_len + 1));
		memcpy(pRegid, pMsg->buf, sizeof(char) * pMsg->regid_len);

		s << "UPDATE DEVICES SET REGID='" << pRegid << "' WHERE DEVICE_IDX = '" << pMsg->nDevice << "';";
		SEND_QUERY(s);

		RESP_RESULT_APPROVED_UDP(CTS_REGID);

		break;
	}	// case CTS_REGID
#pragma endregion
	}
}