#pragma once
#include "Session.h"
#include "singletone.h"

class Server
{
private:
	boost::asio::io_service m_ioserv;
	boost::asio::ip::tcp::acceptor m_Acceptor;

	std::vector<boost::thread*> m_ThreadList;	// thread list
	std::list<Session*>		m_SessionList;	// session list
	unsigned int			m_nSessionCnt;	// session count
	
protected:
				void __post_accept();	// Accept 시작
	__callback	void __handle_accept(Session* _pSession, const boost::system::error_code& _rErr);	// Accept 성공시 핸들함수


	Server();
	~Server(void);
public:
	static Server& GetServer()
	{
		static Server Me;
		return Me;
	}

	void Run(); // 서버 시작
	bool IsValidToken(TOKEN _Device, TOKEN _User);

	void CloseSession(Session* _pSession);			// 세션 종료
	void ProcessPacket(Session* _pSession, const MSG_HEADER* _pMsg);	// 패킷 처리

	boost::asio::io_service& GetIoService()
	{
		return m_ioserv;
	}

	static tm GetDateTimeFromString(const DATETIME_STRING& _rDt);
	// buf는 반드시 공백문자를 포함한 20개짜리 배열이여야 한다.
	static tm GetDateTimeFromString(const char* _pszDatetime);

	static tm NowDateTime();
	static std::string NowDateTimeString();
};
