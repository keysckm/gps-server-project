#pragma once

#define MAX_LOG_BUF 1024

class Log
{
protected:

	enum ErrorType{
		ET_I = 'i',		// 정보
		ET_E = 'e',		// 에러
		ET_W = 'w',		// 경고
		ET_D = 'd',		// 디버그
		ET_V = 'v',		// 상세
	};

	static void __write(string& _rLog, ErrorType _type, bool _IsWriteFile = true);
public:
	Log(void);
	virtual ~Log(void);

	static void e(const TCHAR* _pszLog, ...);	// 에러 출력
	static void w(const TCHAR* _pszLog, ...);	// 경고 출력
	static void i(const TCHAR* _pszLog, ...);	// 정보 출력
	static void d(const TCHAR* _pszLog, ...);	// 디버깅 메시지
	static void v(const TCHAR* _pszLog, ...);	// 디테일 메시지

	static string GetSysErrMsg(int _nErrNo);

	static void GetLogPath(
		_OUT_	TCHAR*	_pszBuf,
				int		_nBufSize);
};

