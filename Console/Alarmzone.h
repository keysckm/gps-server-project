#ifndef __ALARMZONEMODEL_H__
#define __ALARMZONEMODEL_H__

#include "stdafx.h"
#pragma pack(push, 1)

struct ALARMZONE
{
	TOKEN	user;
	double	latitude;
	double	longitude;
	int		distance;
	char	nick[MAX_NICK_LEN];

	ALARMZONE()
	{
		memset(nick, 0, sizeof(char) * MAX_NICK_LEN);
	}

	ALARMZONE(const char* _pUser, const char* _pLat, const char* _pLon, const char* _pDis, const char* _pNick, int _nNicklen)
		: user(_atoi64(_pUser))
		, latitude(atof(_pLat))
		, longitude(atof(_pLon))
		, distance(atoi(_pDis))
	{
		memset(nick, '\0', sizeof(char) * MAX_NICK_LEN);
		memcpy(nick, _pNick, sizeof(char) * _nNicklen);
	}

	ALARMZONE(const int _pUser, const double _dLat, const double _dLon, const int _nDis, const char* _pNick, int _nNicklen)
		: user(_pUser)
		, latitude(_dLat)
		, longitude(_dLon)
		, distance(_nDis)
	{
		memset(nick, '\0', sizeof(char) * MAX_NICK_LEN);
		memcpy(nick, _pNick, sizeof(char) * _nNicklen);
	}

	ALARMZONE(const ALARMZONE& _ref)
	{
		user = _ref.user;
		latitude = _ref.latitude;
		longitude = _ref.longitude;
		distance = _ref.distance;
		memcpy(nick, _ref.nick, sizeof(char) * MAX_NICK_LEN);
	}
};

#pragma pack(pop)
#endif