#pragma once
class DB_UTIL
{
public:
	DB_UTIL();
	~DB_UTIL();

	enum ORDER_BY{
		OB_DESC = 0,
		OB_ASC = 1,
	};

	// insert
	// 테이블, 필드, 데이터를 함께 쓰는 버전
	static string Insert(const char* _pszTable, const char* _pszField, const char* _pszValue)
	{
		SAFE_PTR((_pszTable && _pszField && _pszValue)) "";

		// insert query에 들어가는 고정된 문자열 구문
		const char* pszQuerySTring[3] = { "INSERT INTO ", "(", ") VALUES(" };
		// 조건들
		const char* pszConditionalString[3] = { _pszTable, _pszField, _pszValue };

		string strQuery;
		for (int i = 0; i < 3; i++)
		{
			strQuery += pszQuerySTring[i];
			strQuery += pszConditionalString[i];
		}

		strQuery += ");";

		return strQuery;
	}
	// 테이블, 데이터만 쓰는 버전
	static string Insert(const char* _pszTable, const char* _pszValue)
	{
		SAFE_PTR((_pszTable && _pszValue)) "";

		// insert query에 들어가는 고정된 문자열 구문
		const char* pszQuerySTring[2] = { "INSERT INTO ", " VALUES(" };
		// 조건들
		const char* pszConditionalString[2] = { _pszTable, _pszValue };

		string strQuery;
		for (int i = 0; i < 2; i++)
		{
			strQuery += pszQuerySTring[i];
			strQuery += pszConditionalString[i];
		}

		strQuery += ");";

		return strQuery;
	}

	static string select(const char* _pszColumn, const char* _pszFrom, const char* _pszWhere, const char* _pszOrderby = nullptr, ORDER_BY _order = OB_DESC)
	{
		SAFE_PTR((_pszColumn && _pszFrom && _pszWhere)) "";

		// select query에 들어가는 고정된 문자열 구문
		const char* pszQueryString[4] = { "SELECT ", " FROM ", " WHERE ", " ORDER BY " };
		// 조건들
		const char* pszConditionalString[4] = { _pszColumn, _pszFrom, _pszWhere, _pszOrderby };
		// order by
		const char* pszOrderBy[2] = { "DESC", "ASC" };
		
		string strQuery;
		for (int i = 0; i < 4; i++)
		{
			// 조건문이 존재하지 않으면 무시한다.
			if (!pszConditionalString[i])
				break;

			strQuery += pszQueryString[i];
			strQuery += pszConditionalString[i];
		}

		if (pszConditionalString[3])
		{
			strQuery += " ";
			strQuery += pszOrderBy[_order];
		}

		strQuery += ";";

		return strQuery;
	}
};

