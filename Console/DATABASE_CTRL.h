#pragma once
class DBCtrl : public Singletone<DBCtrl>
{
public:
	const char* CHARSET_UTF8 = "utf8";
	const char* CHARSET_EUCKR = "euckr";
	const char*	DB_HOST = "127.0.0.1";
	const int	DB_PORT = 3306;
	const char* DB_NAME = "project";
	const char* DB_USER = "root";
	const char* DB_PASS = "whrudals";

protected:
	MYSQL*			m_pConnect;
	MYSQL			m_conn;
	MYSQL_RES*		m_pResult;
	MYSQL_ROW		m_Row;
	//MYSQL_FIELD*	m_pFields;
	int				m_nQuery_state;	// 쿼리 통계

	my_ulonglong	m_nResCnt;		// 마지막으로 수행된 쿼리에 대한 result 데이터의 열 갯수
	size_t			m_nFieldCnt;	//						"					 	 필드 갯수

protected:
	friend class Singletone<DBCtrl>;

	void __init();	// sql init

	DBCtrl();
	~DBCtrl();

public:
	bool conn(const char* _sz_db_name = nullptr,
		const char* _sz_db_user = nullptr,
		const char* _sz_db_pass = nullptr,
		const char* _sz_db_host = nullptr,
		const int _n_db_port = 0);	// connect
	bool disconn();
	bool set_charset(const char* _szCharset);
	bool send_query(const char* _szQuery);

	void release_result()
	{
		if (m_pResult)
			mysql_free_result(m_pResult);
		
		m_pResult = nullptr;
		m_Row = nullptr;
		m_nQuery_state = 0;
		m_nResCnt = 0;
		m_nFieldCnt = 0;
	}

	const MYSQL_ROW NextResult()
	{
		SAFE_PTR(m_pResult) nullptr;

		return mysql_fetch_row(m_pResult);
	}

	my_ulonglong GetResultRowCount() const
	{
		return m_nResCnt;
	}

	size_t GetFieldCount() const
	{
		return m_nFieldCnt;
	}

	bool isConnect();
};